import { createContext, useState } from 'react';

const DataContext = createContext({
    active: 'home',
    clientId: '',
    editActive: '',
    clientData: null,
    orderId: '',
    orderData: null,


    changeActive: () => { },
    addClientId: () => { },
    changeEditActive: () => { },
    addClientData: () => { },
    addOrderId: () => { },
    addOrderData: () => { },
});


export const DataContextProvider = (props) => {
    const [active, setActive] = useState('home');
    const [clientId, setClientId] = useState('');
    const [editActive, setEditActive] = useState('');
    const [clientData, setClientData] = useState(null);
    const [orderId, setOrderId] = useState('');
    const [orderData, setOrderData] = useState(null);


    const changeActive = (value) => {
        setActive(value);
    };

    const addClientId = (value) => {
        setClientId(value);
    };

    const changeEditActive = (value) => {
        setEditActive(value);
    };

    const addClientData = (value) => {
        setClientData(value);
    };

    const addOrderId = (value) => {
        setOrderId(value);
    };

    const addOrderData = (value) => {
        setOrderData(value);
    };

    const context = {
        // states 
        active: active,
        clientId: clientId,
        editActive: editActive,
        clientData: clientData,
        orderId: orderId,
        orderData: orderData,

        // functions
        changeActive: changeActive,
        addClientId: addClientId,
        changeEditActive: changeEditActive,
        addClientData: addClientData,
        addOrderId: addOrderId,
        addOrderData: addOrderData
    };


    return (
        <DataContext.Provider value={context}>{props.children}</DataContext.Provider>
    );
};

export default DataContext;