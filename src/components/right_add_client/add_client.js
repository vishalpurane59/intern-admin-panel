import React, { useState, useEffect, useContext } from 'react';
import "./add_client.css";
import addmedia from "../../assets/icons/addmedia.svg";
import axios from 'axios';
import { toast } from "react-toastify";
import DataContext from '../../store/DataProvider';
import Avatar from '../../assets/icons/Avatar.svg';
import close from '../../assets/icons/contactclose.svg';
import FemaleAvatar from '../../assets/icons/FemaleAvatar.svg';

const RightAddClient = () => {
    // eslint-disable-next-line
    const [loading, setLoading] = useState(false);
    const [packages, setPackages] = useState([]);
    const [counter, setCounter] = useState(1);
    const context = useContext(DataContext);
    const [formIndex, setFormIndex] = useState(0);
    const [departments, setDepartments] = useState([]);
    const [contactPersonFeilds, setContactPersonFeilds] = useState([
        {
            role: "",
            name: "",
            contactno: "",
            email: "",
            gender: ""
        }
    ]);

    const [clientDetails, setClientDetails] = useState({
        packageId: "",
        startDate: "",
        companyName: "",
        companyEmail: "",
        contact: "",
        website: "",
        logo: "",
        gstn: "",
        tanNo: "",
        companyAddress: "",
        contactPerson: [],
    });

    const getPackages = async () => {
        try {
            const response = await axios('https://k82gsppxd4.execute-api.ap-south-1.amazonaws.com/latest/api/package');
            if (response.status === 200) {
                setPackages(response.data.data);
            }
        } catch (error) {
            console.log(error);
        }

    };

    const getDepartments = async () => {
        try {
            const response = await axios('https://k82gsppxd4.execute-api.ap-south-1.amazonaws.com/latest/api/department');
            if (response.status === 200) {
                setDepartments(response.data.data);
            }
        } catch (error) {
            console.log(error);
        }
    };

    const createClient = async () => {
        try {
            setLoading(true);
            const body = {
                clientDetails
            };
            const response = await axios.post('https://k82gsppxd4.execute-api.ap-south-1.amazonaws.com/latest/api/client/create', body.clientDetails);
            if (response.status === 200) {
                toast.success("Client created successfully.");
                setClientDetails({
                    packageId: "",
                    startDate: "",
                    companyName: "",
                    companyEmail: "",
                    contact: "",
                    website: "",
                    logo: "",
                    gstn: "",
                    tanNo: "",
                    companyAddress: "",
                    contactPerson: []
                });
                setContactPersonFeilds([
                    {
                        role: "",
                        name: "",
                        contactno: "",
                        email: "",
                        gender: ""
                    }
                ]);
            }
            setLoading(false);
        } catch (error) {
            toast.error("Failed to create client.");
            console.log(error);
        }
    };

    const validateData = () => {
        const isValidEmail = isEmail(clientDetails.companyEmail);
        const isPhoneValid = checkPhoneNumber(clientDetails.contact);
        const isWebsiteValid = checkWebsite(clientDetails.website);
        const isGstnValid = checkGstn(clientDetails.gstn);
        if (isValidEmail === "Invalid Email" || isPhoneValid === "Invalid Phone Number" || clientDetails.companyName === "" || clientDetails.startDate === "" || isWebsiteValid === "Invalid Website") {
            return toast.error(isValidEmail || isPhoneValid || isWebsiteValid || isGstnValid);
        } else {
            createClient();
        }
    };


    const uploadLogoToS3 = async (file) => {
        const uploadURL = await axios.get(`https://k82gsppxd4.execute-api.ap-south-1.amazonaws.com/latest/api/S3Url`);
        await axios.request({
            method: "PUT",
            headers: {
                "Content-Type": file.type
            },
            url: uploadURL.data.uploadURL,
            data: file,
        });
        const imageURL = uploadURL.data.uploadURL.split('?')[0];
        return imageURL;
    };



    const updateClient = async () => {
        try {
            setLoading(true);
            const body = {
                clientDetails
            };
            const response = await axios.put(`https://k82gsppxd4.execute-api.ap-south-1.amazonaws.com/latest/api/client/update/${context.clientId}`, body.clientDetails);
            if (response.status === 200) {
                toast.success('Client updated successfully.');
                context.changeEditActive('');
                setClientDetails({
                    packageId: "",
                    startDate: "",
                    companyName: "",
                    companyEmail: "",
                    contact: "",
                    website: "",
                    logo: "",
                    gstn: "",
                    tanNo: "",
                    companyAddress: "",
                    contactPerson: []
                });
                setContactPersonFeilds([
                    {
                        role: "",
                        name: "",
                        contactno: "",
                        email: "",
                        gender: ""
                    }
                ]);
                setLoading(false);
            }
        } catch (error) {
            toast.error('Failed to update client.');
            console.log(error);
        }
    };

    const cancelEdit = () => {
        context.changeEditActive('');
        context.addClientId("");
        context.changeActive("viewclient");
        setClientDetails({
            packageId: "",
            startDate: "",
            companyName: "",
            companyEmail: "",
            contact: "",
            website: "",
            logo: "",
            gstn: "",
            tanNo: "",
            companyAddress: "",
            contactPersonFeilds: []
        });
        setContactPersonFeilds([{
            role: "",
            name: "",
            contactno: "",
            email: "",
            gender: ""
        }]);
        setCounter(1);
    };

    useEffect(() => {
        getPackages();
        getDepartments();
        if (context.editActive === "client-edit") {
            setClientDetails({
                packageId: context.clientData.packageId?._id,
                startDate: context.clientData.startDate,
                companyName: context.clientData.companyName,
                companyEmail: context.clientData.companyEmail,
                contact: context.clientData.contact,
                website: context.clientData.website,
                logo: context.clientData.logo,
                gstn: context.clientData.gstn,
                tanNo: context.clientData.tanNo,
                companyAddress: context.clientData.companyAddress,
                contactPerson: context.clientData.contactPerson,
            });
            setContactPersonFeilds(context.clientData.contactPerson);
            setCounter(context.clientData.contactPerson.length);
        }
    }, [context.changeEditActive === "client-edit"]); // eslint-disable-line react-hooks/exhaustive-deps

    const addLogo = () => {
        let input = document.createElement('input');
        input.type = 'file';
        input.accept = 'image/*';
        input.onchange = async (e) => {
            const logo = await uploadLogoToS3(e.target.files[0]);
            setClientDetails({ ...clientDetails, logo: logo });
        };
        input.click();
    };

    const handleChange = (name) => (event) => {
        const value = event.target.value;
        setClientDetails({ ...clientDetails, [name]: value });
    };

    const isEmail = (val) => {
        let regEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;//eslint-disable-line
        if (!regEmail.test(val)) {
            return 'Invalid Email';
        }
    };

    const checkPhoneNumber = (no) => {
        let regPhone = /^\d{10}$/;
        if (!regPhone.test(no)) {
            return 'Invalid Phone Number';
        }
    };

    const checkWebsite = (site) => {
        let regSite = /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$/i;
        if (!regSite.test(site)) {
            return 'Invalid Website';
        }
    };

    const checkGstn = (gstn) => {
        let regGstn = /\d{2}[A-Z]{5}\d{4}[A-Z]{1}[A-Z\d]{1}[Z]{1}[A-Z\d]{1}/;
        if (!regGstn.test(gstn)) {
            return 'Invalid GSTN';
        }
    };

    const checkTan = (tan) => {
        let regTan = /(?:(?=(^[a-zA-Z]{5}\d{4}[a-zA-Z]{1}$))|(?=(^[a-zA-Z]{4}[0-9]{5}[a-zA-Z]{1}?$)))/gmi;
        if (!regTan.test(tan)) {
            return 'Invalid Tan Number';
        }
    };

    const handleClick = () => {
        setCounter(counter + 1);
        setContactPersonFeilds([...contactPersonFeilds, {
            role: "",
            name: "",
            contactno: "",
            email: "",
            gender: ""
        }]);
        setFormIndex(contactPersonFeilds.length);
    };


    const handleContactForm = (event, name, index) => {
        const value = event === "Male" || event === "Female" ? event : event.target.value;
        setContactPersonFeilds(current =>
            current.map((obj, i) => {
                if (index === i) {
                    return { ...obj, [name]: value };
                }

                return obj;
            }),
        );
        setClientDetails({ ...clientDetails, contactPerson: contactPersonFeilds });
    };

    const formatDate = (date) => {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        return [year, month, day].join('-');
    };


    const removeContactPerson = (index) => {
        if (contactPersonFeilds.length > 1) {
            setFormIndex(0);
            const filterContactPersons = contactPersonFeilds.filter((data, i) => i !== index);
            setContactPersonFeilds(filterContactPersons);
        } else {
            toast.error("Atleast add one contact person.");
        }
    };


    return (
        <div className="client-container">
            <div className="form-container">
                <div className="heading">
                    <h1>Add Client Form</h1>
                    <div>
                        {context.editActive === "client-edit" ? <button onClick={cancelEdit} style={{
                            marginRight: "20px"
                        }}>Cancel</button> : null}
                        <button onClick={context.editActive === 'client-edit' ? updateClient : validateData}>{context.editActive === 'client-edit' ? "Update" : "Save"}</button>
                    </div>
                </div>

                <div className="client-form">
                    <div className="logo-container">
                        <div className="add-logo" onClick={() => {
                            addLogo();
                        }}>
                            <img src={addmedia} alt="addmedia" />
                            <span>Add logo</span>

                        </div>
                        {clientDetails.logo !== "" && clientDetails.logo !== "No Logo" ? <div className="logo">
                            <img src={clientDetails.logo} alt="" />
                        </div> : null}
                    </div>


                    <div className="form">
                        <div className="input">
                            <span>Package *</span>
                            <select name="package" onChange={handleChange("packageId")}>
                                <option value="">Select Package</option>
                                {packages.map((data, index) => {
                                    return <option value={data._id} key={index} selected={clientDetails.packageId === data._id ? "selected" : null}>{data.name}</option>;
                                })}
                            </select>
                        </div>

                        <div className="input">
                            <span>Date *</span>
                            <input type="date" placeholder='Select your Date' onChange={handleChange("startDate")} value={context.editActive === "client-edit" ? formatDate(clientDetails.startDate) : clientDetails.startDate} />
                        </div>
                        <div className="input">
                            <span>Company *</span>
                            <input type="text" placeholder='Company name here'
                                onChange={handleChange("companyName")}
                                value={clientDetails.companyName} />
                        </div>
                    </div>

                    <div className="form">
                        <div className="input">
                            <span>Email *</span>
                            <input type="text" placeholder='name@work.com'
                                onChange={handleChange("companyEmail")}
                                value={clientDetails.companyEmail} />
                            {clientDetails.companyEmail !== "" ?
                                <span style={{
                                    marginTop: "5px",
                                    color: "red"
                                }}>{isEmail(clientDetails.companyEmail)}</span> : <></>
                            }
                        </div>
                        <div className="input">
                            <span>Phone No. *</span>
                            <input type="number" placeholder='1234567890' onChange={handleChange("contact")}
                                value={clientDetails.contact} maxlength="12" />
                            {clientDetails.contact !== "" ?
                                <span style={{
                                    marginTop: "5px",
                                    color: "red"
                                }}>{checkPhoneNumber(clientDetails.contact)}</span> : <></>
                            }
                        </div>

                        <div className="input">
                            <span>Website *</span>
                            <input type="text" placeholder='https://www.example.com' onChange={handleChange("website")}
                                value={clientDetails.website} />
                            {clientDetails.website !== "" ?
                                <span style={{
                                    marginTop: "5px",
                                    color: "red"
                                }}>{checkWebsite(clientDetails.website)}</span> : <></>
                            }
                        </div>
                    </div>


                    <div className="form2">
                        <div className="input">
                            <span>Address *</span>
                            <textarea name="companyAddress" placeholder='Your address' onChange={handleChange("companyAddress")} value={clientDetails.companyAddress}></textarea>
                        </div>
                        <div className="gstn-container">
                            <div className="input" style={{
                                marginBottom: "30px"
                            }}>
                                <span>GSTN</span>
                                <input type="text" placeholder='18AABCU9603R1ZM' onChange={handleChange("gstn")}
                                    value={clientDetails.gstn} />
                                {clientDetails.gstn !== "" ?
                                    <span style={{
                                        marginTop: "5px",
                                        color: "red"
                                    }}>{checkGstn(clientDetails.gstn)}</span> : <></>
                                }
                            </div>
                            <div className="input">
                                <span>Tan No</span>
                                <input type="text" placeholder='DMPKR9319R' onChange={handleChange("tanNo")}
                                    value={clientDetails.tanNo} />
                                {clientDetails.tanNo !== "" ?
                                    <span style={{
                                        marginTop: "5px",
                                        color: "red"
                                    }}>{checkTan(clientDetails.tanNo)}</span> : <></>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="contact-container">
                <div className="heading">
                    <h1>Add Contact Person</h1>
                </div>

                <div className="form">
                    <div className="input">
                        <span>Name *</span>
                        <input type="text" placeholder='Your name' onChange={(e) => {
                            handleContactForm(e, 'name', formIndex);
                        }} value={contactPersonFeilds[formIndex].name}
                        />
                    </div>
                    <div className="input">
                        <span>Email *</span>
                        <input type="text" placeholder='name@work.com' onChange={(e) => {
                            handleContactForm(e, 'email', formIndex);
                        }} value={contactPersonFeilds[formIndex].email} />
                        {contactPersonFeilds[formIndex].email !== "" ?
                            <span style={{
                                marginTop: "5px",
                                color: "red"
                            }}>{isEmail(contactPersonFeilds[formIndex].email)}</span> : <></>
                        }
                    </div>
                    <div className="input">
                        <span>Phone No. *</span>
                        <input type="number" placeholder='1234567890' onChange={(e) => {
                            handleContactForm(e, 'contactno', formIndex);
                        }} value={contactPersonFeilds[formIndex].contactno} maxlength="12" />
                        {contactPersonFeilds[formIndex].contactno !== "" ?
                            <span style={{
                                marginTop: "5px",
                                color: "red"
                            }}>{checkPhoneNumber(contactPersonFeilds[formIndex].contactno)}</span> : <></>
                        }
                    </div>
                </div>

                <div className="form2">
                    <div className="input">
                        <div className="input">
                            <span>Department *</span>
                            <select name="package" onChange={(e) => {
                                handleContactForm(e, 'role', formIndex);
                            }} defaultValue={contactPersonFeilds[formIndex].role}>
                                <option value="">Select Department</option>
                                {departments.map((data, index) => {
                                    return <option value={data.department} key={index} selected={contactPersonFeilds[formIndex].role === data.department ? "selected" : null}>{data.department}</option>;
                                })}
                            </select>
                        </div>
                        {/* <span>Role *</span>
                        <input type="text" placeholder='Your role' onChange={(e) => {
                            handleContactForm(e, 'role', formIndex);
                        }} value={contactPersonFeilds[formIndex].role} /> */}
                    </div>
                    <div className="gender-buttons">
                        <span>Gender *</span>
                        <div className='buttons'>
                            <button style={contactPersonFeilds[formIndex].gender === "Male" ? { border: "1.2px solid #00C49A" } : null} onClick={() => {
                                handleContactForm("Male", 'gender', formIndex);
                            }}>M</button>
                            <button style={contactPersonFeilds[formIndex].gender === "Female" ? { border: "1.2px solid #00C49A" } : null} onClick={() => {
                                handleContactForm("Female", 'gender', formIndex);
                            }}>F</button>
                        </div>
                    </div>
                </div>
                <div className="button-container">
                    <button onClick={handleClick}>Add</button>
                </div>
            </div>

            <div className="added-contact-container">
                {contactPersonFeilds.length > 0 ? contactPersonFeilds.map((data, index) => {
                    if (contactPersonFeilds[index].contactno !== "" && contactPersonFeilds[index].email !== "" && contactPersonFeilds[index].gender !== "" && contactPersonFeilds[index].name !== "" && contactPersonFeilds[index].role !== "") {
                        return (
                            <div className="contact-person-card" key={index} style={formIndex === index ? { border: '1px solid red', } : null} onClick={() => {
                                setFormIndex(index);
                            }}>
                                <div className="heading">
                                    <img src={close} alt="close" onClick={(e) => {
                                        e.stopPropagation();
                                        e.preventDefault();
                                        removeContactPerson(index);
                                    }} />
                                </div>
                                <img src={data.gender === "Male" ? Avatar : FemaleAvatar} alt="avatar" />
                                <span className='name'>{data.name}</span>
                                <span>{data.role}</span>
                                <span>{data.contactno}</span>
                                <span>{data.email}</span>
                            </div>
                        );
                    } else {
                        return "";
                    }
                }) : null}

            </div>
        </div >
    );
};

export default RightAddClient;