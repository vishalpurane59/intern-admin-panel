import React, { useState, useEffect } from 'react';
import "./RightAddJobRole.css";
import axios from 'axios';
import { toast } from "react-toastify";
import trash from "../../assets/icons/trash.svg";
import tag from "../../assets/icons/tag.svg";
import view from "../../assets/icons/View.svg";
import edit from "../../assets/icons/edit.svg";
import EditRolePopup from '../edit_role_popup/EditRolePopup';
import ViewRolePopup from '../view_role_popup/ViewRolePopup';

const RightAddJobRole = () => {
    const [loading, setLoading] = useState(false);
    const [isEditActive, setIsEditActive] = useState(false);
    const [skill, setSkill] = useState([]);
    const [role, setRole] = useState([]);
    const [roleId, setRoleId] = useState("");
    const [searchSkill, setSearchSkill] = useState([]);
    const [search, setSearch] = useState("");
    const [userSkills, setUserSkills] = useState([]);
    const [viewData, setViewData] = useState(null);
    const [isViewActive, setIsViewActive] = useState(false);
    const [roleLoading, setRoleLoading] = useState(false);

    const [roleDetails, setRoleDetails] = useState({
        jobrole: "",
        skills: []
    });

    useEffect(() => {
        getSkills();
        getJobRoles();
    }, []);


    const searchSkills = (value) => {
        let filteredSkill = skill.filter((skill) => skill.skills.toLowerCase().includes(value.trim().toLowerCase()));
        if (filteredSkill.length === 0) {
            setSearchSkill([]);
        } else {
            setSearchSkill(filteredSkill);
        }
    };

    const handleSearchChange = (e) => {
        searchSkills(e.target.value)
        setSearch(e.target.value)
    }

    const getJobRoles = async () => {
        try {
            setRoleLoading(true);
            const response = await axios('https://k82gsppxd4.execute-api.ap-south-1.amazonaws.com/latest/api/jobrole/getroleTable');
            if (response.status === 200) {
                setRole(response.data.roleTables);
                setRoleLoading(false);
            }
        } catch (error) {
            setRoleLoading(false);
            toast.error("Failed to get roles");
        }
    };

    const getSkills = async () => {
        try {
            const response = await axios('https://k82gsppxd4.execute-api.ap-south-1.amazonaws.com/latest/api/skill/getskill');
            if (response.data !== undefined) {
                setSkill(response.data.skills);
            }
        } catch (error) {
            toast.error("Failed to get skills");
        }
    };

    const createJobRole = async () => {
        try {
            setLoading(true);
            const body = {
                roleDetails
            };
            const response = await axios.post('https://k82gsppxd4.execute-api.ap-south-1.amazonaws.com/latest/api/jobrole/createroleTable', body.roleDetails);
            setRole((prev) => [response.data.roleTable, ...prev])
            toast.success("Role created successfully.");
            setRoleDetails({
                jobrole: "",
                skills: []
            });
            setLoading(false);
        } catch (error) {
            toast.error("Failed to create job role.");
            setLoading(false)
        }
    };



    const updateRole = async () => {
        try {
            setLoading(true);
            const body = {
                roleDetails
            };

            await axios.put(`https://k82gsppxd4.execute-api.ap-south-1.amazonaws.com/latest/api/jobrole/update/${roleId}`, body.roleDetails);
                toast.success('Role updated successfully.');
                setIsEditActive(false);
                setSearch("")
                setRoleDetails({
                    jobrole: "",
                    skills: []
                });
                setLoading(false);
                getJobRoles();
        } catch (error) {
            toast.error('Failed to update role.');
        }
    };

    const cancelEdit = () => {
        setIsEditActive(false);
        setRoleId("");
        setRoleDetails({
            jobrole: "",
            skills: []
        });
        setSearch("")
        setUserSkills([]);
    };

    const handleEdit = (data) => {
        setIsEditActive(true);
        setRoleId(data._id);
        setRoleDetails({
            jobrole: data.jobrole,
            skills: data.skills
        });
        setUserSkills(data.skills);
    };

    const handleChange = (name) => (event) => {
        const value = event.target.value;
        if (name === "jobrole") {
            setRoleDetails({ ...roleDetails, jobrole: value });
        } else {
            let skillArray = [{ skillId: value }];
            setRoleDetails({ ...roleDetails, skills: skillArray });
        }
    };


    const deleteRole = async (id) => {
        try {
            await axios.delete(`https://k82gsppxd4.execute-api.ap-south-1.amazonaws.com/latest/api/jobrole/deleteroleTable/${id}`);
            toast.error('Role deleted successfully.');
            let updatedData = role.filter(data => data._id !== id)
            setRole(updatedData)
        } catch (error) {
            toast.error('Failed to delete role.');
        }
    };

    const addSkill = (data) => {
        let obj = { skillId: data };
        setRoleDetails({ ...roleDetails, skills: [...roleDetails.skills, { skillId: data, _id: data._id }] });
        setUserSkills([...userSkills, obj]);
    };

    const removeSkills = (data) => {
        let itemRemoved = roleDetails.skills.filter((skill) => skill.skillId._id !== data.skillId._id);
        setRoleDetails({ ...roleDetails, skills: itemRemoved });
        setUserSkills(itemRemoved);
    };

    const handleView = (data) => {
        setIsViewActive(true);
        setViewData(data);
    };

    const validateData = () => {
        if (roleDetails.jobrole === "") {
            toast.error("Role cannot be empty");
        } else {
            createJobRole();
        }
    };

    const closeView = () => {
        setIsViewActive(false);
        setViewData(null);
    }


    return (
        <div className="add-role-container">
            <div className="add-topic-form-container">
                <div className="heading">
                    <div className='title'>
                        <div className="sidebar"></div>
                        <span>Add Role</span>
                    </div>

                    <div className="form">
                        <input type="text" placeholder='Enter role here' name="jobrole"
                            onChange={handleChange("jobrole")}
                            value={isEditActive ? "" : roleDetails.jobrole}
                        />
                        <button onClick={() => {
                            validateData();
                        }}>{loading ? <i class="fa fa-circle-o-notch fa-spin"></i> : "+"}</button>
                    </div>
                </div>
            </div>

            <div className="list-topic">
                <div className="heading">
                    <div className="sidebar"></div>
                    <span>Added Roles</span>
                </div>
                {roleLoading ? <div className='loader-container'>
                    <i class="fa fa-circle-o-notch fa-spin fa-5x" style={{
                        color: "#FF6813"
                    }}></i>
                </div> : <div className="topic-container">
                    <table>
                        <tr>
                            <th>Sr.No</th>
                            <th>Role</th>
                            <th>Assign Skills</th>
                            <th>Options</th>
                        </tr>

                        {role.map((data, index) => {
                            return (
                                <tr>
                                    <td>{index + 1}</td>
                                    <td>{data.jobrole}</td>
                                    <td onClick={() => {
                                        handleView(data);
                                    }} style={{
                                        cursor: "pointer"
                                    }}>
                                        <div className="assign-skills">
                                            {data.skills.length === 0 ? <button onClick={(e) => {
                                                e.stopPropagation();
                                                e.preventDefault();
                                                handleEdit(data);
                                            }}>Assign</button> : (
                                                <div className="skills" style={{ cursor: "pointer" }} >
                                                    <img src={tag} alt="skill" />
                                                    {
                                                        data.skills.map((skill, index) => {
                                                            if (index < 3) {
                                                                return <span style={{ marginRight: 5 }} key={index}>{skill.skillId?.skills}{index === 3 || (data.skills.length - 1) === index ? null : ","}</span>
                                                            } else {
                                                                return null
                                                            }
                                                        })
                                                    }
                                                    {data.skills.length > 3 ? <span>{`+${data.skills.length - 3} more`}</span> : null}
                                                </div>
                                            )}
                                        </div>
                                    </td>
                                    <td>
                                        <div className="options">
                                            <div className='edit' onClick={() => handleEdit(data)}>
                                                <img src={edit} alt="edit" />
                                            </div>

                                            <div className='delete' onClick={() => deleteRole(data._id)}>
                                                <img src={trash} alt="trash" />
                                            </div>

                                            <div className='view' onClick={() => handleView(data)}>
                                                <img src={view} alt="view" />
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            );
                        })}

                    </table>
                </div>}
            </div>

            {
                isEditActive ? <EditRolePopup
                    cancelEdit={cancelEdit}
                    updateRole={updateRole}
                    role={roleDetails.jobrole}
                    search={search}
                    handleChange={handleChange}
                    handleSearchChange={handleSearchChange}
                    addSkill={addSkill}
                    searchSkill={searchSkill}
                    userSkills={userSkills}
                    removeSkills={removeSkills}
                    loading={loading} /> : null
            }

            {isViewActive ? <ViewRolePopup viewData={viewData} role={viewData.jobrole} closeView={closeView} /> : null}
        </div>
    );
};

export default RightAddJobRole;