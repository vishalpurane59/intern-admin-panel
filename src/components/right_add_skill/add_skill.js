import React, { useState, useEffect } from 'react';
import "./add_skill.css";
import trash from "../../assets/icons/trash.svg";
import axios from 'axios';
import { toast } from "react-toastify";
import close from "../../assets/icons/close.svg";
import tag from "../../assets/icons/tag.svg";
import view from "../../assets/icons/View.svg";
import edit from "../../assets/icons/edit.svg";


const RightAddSkill = () => {
    const [loading, setLoading] = useState(false);
    const [isEditActive, setIsEditActive] = useState(false);
    const [skillId, setSkillId] = useState("");
    const [skill, setSkill] = useState([]);
    const [topics, setTopics] = useState([]);
    const [searchTopic, setSearchTopic] = useState([]);
    const [search, setSearch] = useState("");
    const [userTopic, setUserTopic] = useState([]);
    const [viewData, setViewData] = useState(null);
    const [isViewActive, setIsViewActive] = useState(false);
    const [skillLoading, setSkillLoading] = useState(false);

    const [skillDetails, setSkillDetails] = useState({
        skills: "",
        topics: [],
    });


    const searchTopics = (value) => {
        let filteredTopic = topics.filter((topic) => topic.topic.toLowerCase().includes(value.trim().toLowerCase()));
        if (filteredTopic.length === 0) {
            setSearchTopic([{
                topic: "No topic found."
            }]);
        } else {
            setSearchTopic(filteredTopic);
        }
    };

    const getSkills = async () => {
        try {
            setSkillLoading(true);
            const response = await axios('https://k82gsppxd4.execute-api.ap-south-1.amazonaws.com/latest/api/skill/getskill');
            if (response.data !== undefined) {
                setSkill(response.data.skills);
                setSkillLoading(false);
            }
        } catch (error) {
            console.log(error);
        }
    };

    const getTopics = async () => {
        try {
            const response = await axios('https://k82gsppxd4.execute-api.ap-south-1.amazonaws.com/latest/api/topic/gettopic');
            if (response.data !== undefined) {
                setTopics(response.data.topics);
            }
        } catch (error) {
            console.log(error);
        }
    };


    const createSkill = async () => {
        try {
            setLoading(true);
            const body = {
                skillDetails
            };
            const response = await axios.post('https://k82gsppxd4.execute-api.ap-south-1.amazonaws.com/latest/api/skill/createskill', body.skillDetails);
            if (response.status === 200) {
                toast.success("Skill created successfully.");
                setSkillDetails({
                    skills: "",
                    topics: []
                });
                getSkills();
            }
            setLoading(false);
        } catch (error) {
            toast.error('Failed to create skill.');
            setLoading(false);
            console.log(error);
        }
    };


    const deleteSkill = async (id) => {
        try {
            const response = await axios.delete(`https://k82gsppxd4.execute-api.ap-south-1.amazonaws.com/latest/api/skill/deleteskill/${id}`);
            if (response.status === 200) {
                toast.error('Skill deleted successfully.');
                getSkills();
            }
        } catch (error) {
            toast.error('Failed to delete skill.');
            console.log(error);
        }
    };

    const updateSkill = async () => {
        try {
            setLoading(true);
            const body = {
                skillDetails
            };
            const response = await axios.patch(`https://k82gsppxd4.execute-api.ap-south-1.amazonaws.com/latest/api/skill/update/${skillId}`, body.skillDetails);
            if (response.status === 200) {
                toast.success('Skill updated successfully.');
                setIsEditActive(false);
                setSkillDetails({
                    skills: "",
                    topics: []
                });
                getSkills();
                setLoading(false);
            }
        } catch (error) {
            setLoading(false);
            toast.error('Failed to update skill.');
            console.log(error);
        }
    };

    const handleEdit = (data) => {
        setIsEditActive(true);
        setSkillId(data._id);
        setSkillDetails({
            skills: data.skills,
            topics: data.topics
        });
        setUserTopic(data.topics);
    };


    const cancelEdit = () => {
        setIsEditActive(false);
        setSkillId("");
        setSkillDetails({
            skills: "",
            topics: []
        });
        setUserTopic([]);
    };


    useEffect(() => {
        getSkills();
        getTopics();
    }, []);

    const handleChange = (name) => (event) => {
        const value = event.target.value;
        setSkillDetails({ ...skillDetails, [name]: value });
    };

    const addTopic = (data) => {
        let obj = { topicId: data };
        const isTopicPresent = userTopic.find((topic) => topic.topicId._id === data._id);
        if (!isTopicPresent) {
            setSkillDetails({ ...skillDetails, topics: [...skillDetails.topics, { topicId: data._id }] });
            setUserTopic([...userTopic, obj]);
        }
    };

    const removeTopic = (data) => {
        let itemRemoved = skillDetails.topics.filter((topic) => topic._id !== data._id);
        setSkillDetails({ ...skillDetails, topics: itemRemoved });
        setUserTopic(itemRemoved);
    };

    const handleView = (data) => {
        setIsViewActive(true);
        setViewData(data);
    };


    const editPopUp = () => {
        const filteredTopic = userTopic.filter((data) => data.topicId !== null);
        return (
            <div className="edit-container">
                <div className="heading">
                    <div className="left-side">
                        <div className="sidebar"></div>
                        <span>Edit Skill</span>
                    </div>
                    <img src={close} alt="close" onClick={() => {
                        cancelEdit();
                    }} />
                </div>
                <div className="form">
                    <span>Selected Skill</span>
                    <input type="text" placeholder='Skill Name' value={skillDetails.skills} name="skills"
                        onChange={handleChange("skills")} />
                    <br />
                    <br />
                    <span>Assign Topics</span>
                    <input type="text" placeholder='Search Topics' className='search' onChange={(e) => {
                        searchTopics(e.target.value);
                        setSearch(e.target.value);
                    }} />
                    {search !== "" && searchTopic.length !== 0 ? <div className="search-skill-container">
                        {searchTopic.map((data) => {
                            return <span onClick={() => {
                                addTopic(data);
                            }}>{data.topic}</span>;
                        })}
                    </div> : null}

                    {filteredTopic.length !== 0 ? <div className="skill-container">
                        {filteredTopic.map((data) => {
                            return <div className='button'>
                                {data.topicId.topic}
                                <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg" onClick={() => {
                                    removeTopic(data);
                                }}>
                                    <circle cx="10" cy="10" r="10" fill="#E2E8EB" />
                                    <path d="M13 7L7 13" stroke="#FF6812" stroke-linecap="round" />
                                    <path d="M13 13L7.00019 7.00019" stroke="#FF6812" stroke-linecap="round" />
                                </svg>
                            </div>;
                        })}
                    </div> : null}
                </div>
                <div className="btn-container">
                    <button onClick={() => {
                        updateSkill();
                    }}>{loading ? "Loading..." : "Update"}</button>
                </div>
            </div>
        );
    };

    const viewSkillPopUp = () => {
        const filteredTopic = viewData.topics.filter((data) => data.topicId !== null);
        return (
            <div className="edit-container" style={{
                top: "30%"
            }}>
                <div className="heading">
                    <div className="left-side">
                        <div className="sidebar"></div>
                        <span>View Skill</span>
                    </div>
                    <img src={close} alt="close" onClick={() => {
                        setIsViewActive(false);
                        setViewData(null);
                    }} />
                </div>
                <div className="form">
                    <span>Skill</span>
                    <input type="text" placeholder='Skill Name' value={viewData.skills} disabled />
                    <br />
                    <br />
                    <span>Topics</span>
                    {filteredTopic.length !== 0 ? <div className="skill-container">
                        {filteredTopic.map((data) => {
                            return <div className='button' style={{
                                display: "flex",
                                justifyContent: "center",
                            }}>
                                {data.topicId.topic}
                            </div>;
                        })}
                    </div> : <span style={{
                        color: "#696974"
                    }}>No topics</span>}
                </div>
            </div>
        );
    };

    const validateData = () => {
        if (skillDetails.skills === "") {
            toast.error("Skill cannot be empty");
        } else {
            createSkill();
        }
    };


    return (
        <div className="add-skill-container">
            <h1>Skills</h1>
            <div className="add-topic-form-container">
                <div className="heading">
                    <div className="sidebar"></div>
                    <span>Add Skills</span>
                </div>
                <span className="sub-heading">Skills can be use to define roles</span>

                <div className="form">
                    <input type="text" placeholder='Skill Here' name="skills"
                        onChange={handleChange("skills")}
                        value={isEditActive ? "" : skillDetails.skills}
                    />
                    <button onClick={() => {
                        validateData();
                    }}>{loading ? <i class="fa fa-circle-o-notch fa-spin"></i> : "+"}</button>
                </div>
            </div>
            <div className="list-topic">
                <div className="heading">
                    <div className="sidebar"></div>
                    <span>Added Skills</span>
                </div>
                {skillLoading ? <div style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    marginTop: "100px"
                }}>
                    <i class="fa fa-circle-o-notch fa-spin fa-5x" style={{
                        color: "#FF6813"
                    }}></i>
                </div> : <div className="topic-container">
                    <table>
                        <tr>
                            <th>SR.NO</th>
                            <th>Skill</th>
                            <th>Assign Topics</th>
                            <th>OPTIONS</th>
                        </tr>

                        {skill.map((data, index) => {
                            return (
                                <tr>
                                    <td>{index + 1}</td>
                                    <td>{data.skills}</td>
                                    <td onClick={() => {
                                        handleView(data);
                                    }} style={{
                                        cursor: "pointer"
                                    }}>
                                        <div className="assign-skills">
                                            {data.topics.length === 0 ? <button onClick={(e) => {
                                                e.stopPropagation();
                                                e.preventDefault();
                                                handleEdit(data);
                                            }}>Assign</button> : (
                                                <div className="skills" >
                                                    <img src={tag} alt="skill" />
                                                    <span>{data.topics[0].topicId?.topic || data.topics[1].topicId?.topic || data.topics[2].topicId?.topic || data.topics[3].topicId?.topic}  {data.topics.length === 1 ? null : `+ ${data.topics.length - 1} more`}  </span>
                                                </div>
                                            )}
                                        </div>
                                    </td>
                                    <td>
                                        <div className="options">
                                            <img src={edit} alt="trash" onClick={() => {
                                                handleEdit(data);
                                            }} />
                                            <img src={trash} alt="trash" onClick={() => {
                                                deleteSkill(data._id);
                                            }} />
                                            <img src={view} alt="view" onClick={() => {
                                                handleView(data);
                                            }} />
                                        </div>
                                    </td>
                                </tr>
                            );
                        })}

                    </table>
                </div>}
            </div>

            {isEditActive ? editPopUp() : null}

            {isViewActive ? viewSkillPopUp() : null}

        </div>
    );
};
export default RightAddSkill;