import React, { useContext } from "react";
import "./Sidebar.css";
import { useNavigate } from "react-router-dom";
import DataContext from "../../store/DataProvider";

// assets
import navbarLine from "../../assets/icons/navbarLine.svg";
import navbarTwoLines from "../../assets/icons/navbarTwoLines.svg";

const Sidebar = () => {
    const context = useContext(DataContext);
    let navigate = useNavigate();

    const logout = () => {
        localStorage.removeItem("token");
        navigate("/");
    };

    const handleDropDown = (value) => {
        if (context.active === value) {
            context.changeActive("home");
        } else {
            context.changeActive(value);
        }
    };

    return (
        <div className="sidebar-container">
            <div className="button-container">
                <div
                    className={context.active === "home" ? "active-button" : "button"}
                    onClick={() => {
                        context.changeEditActive('');
                        context.addClientId("");
                        context.addClientData(null);
                        context.addOrderData(null);
                        context.addOrderId("");
                        handleDropDown("home");
                    }}
                >
                    <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                        <path
                            fill-rule="evenodd"
                            clip-rule="evenodd"
                            d="M2.99311 9.10625C2.36096 9.67518 2 10.4857 2 11.3361V19C2 20.6569 3.34315 22 5 22H19C20.6569 22 22 20.6569 22 19V11.3361C22 10.4857 21.639 9.67518 21.0069 9.10625L14.0069 2.80625C12.866 1.77943 11.134 1.77943 9.9931 2.80625L2.99311 9.10625ZM11 13C9.89543 13 9 13.8955 9 15V19C9 19.5523 9.44772 20 10 20H14C14.5523 20 15 19.5523 15 19V15C15 13.8955 14.1046 13 13 13H11Z"
                            fill={context.active === "home" ? "#1A1D1F" : "#6F767E"}
                        />
                    </svg>
                    <span>Home</span>
                </div>
                <div
                    className={
                        context.active === "manageclient" ||
                            context.active === "onboardclient" ||
                            context.active === "viewclient" ||
                            context.active === "viewtimeline"
                            ? "active-button"
                            : "button"
                    }
                    onClick={() => {
                        handleDropDown("manageclient");
                    }}
                >
                    <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                        <path
                            fill-rule="evenodd"
                            clip-rule="evenodd"
                            d="M12 12C13.1046 12 14 11.1046 14 10C14 8.89543 13.1046 8 12 8C10.8954 8 10 8.89543 10 10C10 11.1046 10.8954 12 12 12ZM12 14C14.2091 14 16 12.2091 16 10C16 7.79086 14.2091 6 12 6C9.79086 6 8 7.79086 8 10C8 12.2091 9.79086 14 12 14Z"
                            fill={
                                context.active === "manageclient" ||
                                    context.active === "onboardclient" ||
                                    context.active === "viewclient" ||
                                    context.active === "viewtimeline"
                                    ? "#1A1D1F"
                                    : "#6F767E"
                            }
                        />
                        <path
                            fill-rule="evenodd"
                            clip-rule="evenodd"
                            d="M18.5588 19.5488C20.6672 17.7154 22 15.0134 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 15.0134 3.33284 17.7154 5.44116 19.5488C7.19693 21.0756 9.49052 22 12 22C14.4162 22 16.6323 21.143 18.3609 19.7165C18.4276 19.6614 18.4936 19.6055 18.5588 19.5488ZM12.2579 19.9959C12.1723 19.9986 12.0863 20 12 20C11.9914 20 11.9827 20 11.9741 20C11.8937 19.9997 11.8135 19.9983 11.7337 19.9956C10.3914 19.9517 9.13273 19.5772 8.03655 18.9508C8.95181 17.7632 10.3882 17 12 17C13.6118 17 15.0482 17.7632 15.9634 18.9508C14.865 19.5785 13.6033 19.9533 12.2579 19.9959ZM17.5624 17.7498C16.2832 16.0781 14.2675 15 12 15C9.73249 15 7.7168 16.0781 6.43759 17.7498C4.93447 16.2953 4 14.2568 4 12C4 7.58172 7.58172 4 12 4C16.4183 4 20 7.58172 20 12C20 14.2568 19.0655 16.2953 17.5624 17.7498Z"
                            fill={
                                context.active === "manageclient" ||
                                    context.active === "onboardclient" ||
                                    context.active === "viewclient" ||
                                    context.active === "viewtimeline"
                                    ? "#1A1D1F"
                                    : "#6F767E"
                            }
                        />
                    </svg>
                    <span>Manage Client</span>
                    <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                        style={{
                            marginLeft: "90px",
                        }}
                    >
                        <path
                            d="M6.70711 8.29289C6.31658 7.90237 5.68342 7.90237 5.29289 8.29289C4.90237 8.68342 4.90237 9.31658 5.29289 9.70711L6.70711 8.29289ZM12 15L11.2929 15.7071C11.6834 16.0976 12.3166 16.0976 12.7071 15.7071L12 15ZM18.7071 9.70711C19.0976 9.31658 19.0976 8.68342 18.7071 8.29289C18.3166 7.90237 17.6834 7.90237 17.2929 8.29289L18.7071 9.70711ZM5.29289 9.70711L11.2929 15.7071L12.7071 14.2929L6.70711 8.29289L5.29289 9.70711ZM12.7071 15.7071L18.7071 9.70711L17.2929 8.29289L11.2929 14.2929L12.7071 15.7071Z"
                            fill="#6F767E"
                        />
                    </svg>
                </div>

                {context.active === "manageclient" ||
                    context.active === "onboardclient" ||
                    context.active === "viewclient" ||
                    context.active === "viewtimeline" ? (
                    <div className="navbarLine">
                        <div>
                            <img src={navbarLine} alt="" style={{ height: "100%" }} />
                        </div>
                        <div className="collapse">
                            <span
                                onClick={() => {
                                    handleDropDown("onboardclient");
                                }}
                                style={
                                    context.active === "onboardclient" ||
                                        context.active === "manageclient"
                                        ? { color: "#FF6812" }
                                        : null
                                }
                            >
                                Onboard
                            </span>
                            <span
                                onClick={() => {
                                    handleDropDown("viewclient");
                                }}
                                style={
                                    context.active === "viewclient" ? { color: "#FF6812" } : null
                                }
                            >
                                View
                            </span>
                            <span
                                onClick={() => {
                                    handleDropDown("viewtimeline");
                                }}
                                style={
                                    context.active === "viewtimeline"
                                        ? { color: "#FF6812" }
                                        : null
                                }
                            >
                                View Timeline
                            </span>
                        </div>
                    </div>
                ) : null}

                <div
                    className={
                        context.active === "manageorders" ||
                            context.active === "generateorders" ||
                            context.active === "vieworders"
                            ? "active-button"
                            : "button"
                    }
                    onClick={() => {
                        context.changeEditActive('');
                        context.addClientId("");
                        context.addClientData(null);
                        context.addOrderData(null);
                        context.addOrderId("");
                        handleDropDown("manageorders");
                    }}
                >
                    <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                        <path
                            d="M4 19.5C4 18.837 4.26339 18.2011 4.73223 17.7322C5.20107 17.2634 5.83696 17 6.5 17H20"
                            stroke={
                                context.active === "manageorders" ||
                                    context.active === "generateorders" ||
                                    context.active === "vieworders"
                                    ? "#1A1D1F"
                                    : "#6F767E"
                            }
                            stroke-width="2"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                        />
                        <path
                            d="M6.5 2H20V22H6.5C5.83696 22 5.20107 21.7366 4.73223 21.2678C4.26339 20.7989 4 20.163 4 19.5V4.5C4 3.83696 4.26339 3.20107 4.73223 2.73223C5.20107 2.26339 5.83696 2 6.5 2V2Z"
                            stroke={
                                context.active === "manageorders" ||
                                    context.active === "generateorders" ||
                                    context.active === "vieworders"
                                    ? "#1A1D1F"
                                    : "#6F767E"
                            }
                            stroke-width="2"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                        />
                    </svg>
                    <span>Manage Orders</span>
                    <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                        style={{
                            marginLeft: "85px",
                        }}
                    >
                        <path
                            d="M6.70711 8.29289C6.31658 7.90237 5.68342 7.90237 5.29289 8.29289C4.90237 8.68342 4.90237 9.31658 5.29289 9.70711L6.70711 8.29289ZM12 15L11.2929 15.7071C11.6834 16.0976 12.3166 16.0976 12.7071 15.7071L12 15ZM18.7071 9.70711C19.0976 9.31658 19.0976 8.68342 18.7071 8.29289C18.3166 7.90237 17.6834 7.90237 17.2929 8.29289L18.7071 9.70711ZM5.29289 9.70711L11.2929 15.7071L12.7071 14.2929L6.70711 8.29289L5.29289 9.70711ZM12.7071 15.7071L18.7071 9.70711L17.2929 8.29289L11.2929 14.2929L12.7071 15.7071Z"
                            fill="#6F767E"
                        />
                    </svg>
                </div>

                {context.active === "manageorders" ||
                    context.active === "generateorders" ||
                    context.active === "vieworders" ? (
                    <div className="navbarLine" style={{ marginBottom: "10px" }} >
                        <div>
                            <img
                                src={navbarTwoLines}
                                alt=""
                                style={{ height: "100%", marginLeft: "20px" }}
                            />
                        </div>
                        <div
                            className="collapse"
                            style={{
                                height: "90px",
                            }}
                        >
                            <span
                                onClick={() => {
                                    handleDropDown("generateorders");
                                }}
                                style={
                                    context.active === "generateorders" ||
                                        context.active === "manageorders"
                                        ? { color: "#FF6812", marginTop: "18px" }
                                        : { marginTop: "18px" }
                                }
                            >
                                Generate
                            </span>
                            <span
                                onClick={() => {
                                    handleDropDown("vieworders");
                                }}
                                style={
                                    context.active === "vieworders"
                                        ? { color: "#FF6812", marginTop: "33px" }
                                        : { marginTop: "33px" }
                                }
                            >
                                View
                            </span>
                        </div>
                    </div>
                ) : null}

                <div
                    className={
                        context.active === "librarysettings" ||
                            context.active === "roles" ||
                            context.active === "skills" ||
                            context.active === "topics"
                            ? "active-button"
                            : "button"
                    }
                    onClick={() => {
                        context.changeEditActive('');
                        context.addClientId("");
                        context.addClientData(null);
                        context.addOrderData(null);
                        context.addOrderId("");
                        handleDropDown("librarysettings");
                    }}
                >
                    <svg
                        width="20"
                        height="20"
                        viewBox="0 0 20 20"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                        <g clip-path="url(#clip0_759_292)">
                            <path
                                d="M9.16663 3.33331H3.33329C2.89127 3.33331 2.46734 3.50891 2.15478 3.82147C1.84222 4.13403 1.66663 4.55795 1.66663 4.99998V16.6666C1.66663 17.1087 1.84222 17.5326 2.15478 17.8452C2.46734 18.1577 2.89127 18.3333 3.33329 18.3333H15C15.442 18.3333 15.8659 18.1577 16.1785 17.8452C16.491 17.5326 16.6666 17.1087 16.6666 16.6666V10.8333"
                                stroke={
                                    context.active === "librarysettings" ||
                                        context.active === "roles" ||
                                        context.active === "skills" ||
                                        context.active === "topics"
                                        ? "#1A1D1F"
                                        : "#6F767E"
                                }
                                stroke-width="2"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                            />
                            <path
                                d="M15.4166 2.08332C15.7481 1.7518 16.1978 1.56555 16.6666 1.56555C17.1355 1.56555 17.5851 1.7518 17.9166 2.08332C18.2481 2.41484 18.4344 2.86448 18.4344 3.33332C18.4344 3.80216 18.2481 4.2518 17.9166 4.58332L9.99996 12.5L6.66663 13.3333L7.49996 9.99999L15.4166 2.08332Z"
                                stroke={
                                    context.active === "librarysettings" ||
                                        context.active === "roles" ||
                                        context.active === "skills" ||
                                        context.active === "topics"
                                        ? "#1A1D1F"
                                        : "#6F767E"
                                }
                                stroke-width="2"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                            />
                        </g>
                        <defs>
                            <clipPath id="clip0_759_292">
                                <rect width="20" height="20" fill="white" />
                            </clipPath>
                        </defs>
                    </svg>
                    <span>Library Settings</span>
                    <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                        style={{
                            marginLeft: "88px",
                        }}
                    >
                        <path
                            d="M6.70711 8.29289C6.31658 7.90237 5.68342 7.90237 5.29289 8.29289C4.90237 8.68342 4.90237 9.31658 5.29289 9.70711L6.70711 8.29289ZM12 15L11.2929 15.7071C11.6834 16.0976 12.3166 16.0976 12.7071 15.7071L12 15ZM18.7071 9.70711C19.0976 9.31658 19.0976 8.68342 18.7071 8.29289C18.3166 7.90237 17.6834 7.90237 17.2929 8.29289L18.7071 9.70711ZM5.29289 9.70711L11.2929 15.7071L12.7071 14.2929L6.70711 8.29289L5.29289 9.70711ZM12.7071 15.7071L18.7071 9.70711L17.2929 8.29289L11.2929 14.2929L12.7071 15.7071Z"
                            fill="#6F767E"
                        />
                    </svg>
                </div>

                {context.active === "librarysettings" ||
                    context.active === "roles" ||
                    context.active === "skills" ||
                    context.active === "topics" ? (
                    <div className="navbarLine">
                        <div>
                            <img src={navbarLine} alt="" style={{ height: "100%" }} />
                        </div>
                        <div
                            className="collapse"
                            style={{
                                height: "130px",
                            }}
                        >
                            <span
                                onClick={() => {
                                    handleDropDown("roles");
                                }}
                                style={
                                    context.active === "roles" ||
                                        context.active === "librarysettings"
                                        ? { color: "#FF6812" }
                                        : null
                                }
                            >
                                Roles
                            </span>
                            <span
                                onClick={() => {
                                    handleDropDown("skills");
                                }}
                                style={
                                    context.active === "skills" ? { color: "#FF6812" } : null
                                }
                            >
                                Skills
                            </span>

                            <span
                                onClick={() => {
                                    handleDropDown("topics");
                                }}
                                style={
                                    context.active === "topics" ? { color: "#FF6812" } : null
                                }
                            >
                                Topics
                            </span>
                        </div>
                    </div>
                ) : null}

                <div
                    className={
                        context.active === "managestaff" ? "active-button" : "button"
                    }
                    onClick={() => {
                        context.changeEditActive('');
                        context.addClientId("");
                        context.addClientData(null);
                        context.addOrderData(null);
                        context.addOrderId("");
                        handleDropDown("managestaff");
                    }}
                >
                    <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                        <path
                            fill-rule="evenodd"
                            clip-rule="evenodd"
                            d="M12 12C13.1046 12 14 11.1046 14 10C14 8.89543 13.1046 8 12 8C10.8954 8 10 8.89543 10 10C10 11.1046 10.8954 12 12 12ZM12 14C14.2091 14 16 12.2091 16 10C16 7.79086 14.2091 6 12 6C9.79086 6 8 7.79086 8 10C8 12.2091 9.79086 14 12 14Z"
                            fill={context.active === "managestaff" ? "#1A1D1F" : "#6F767E"}
                        />
                        <path
                            fill-rule="evenodd"
                            clip-rule="evenodd"
                            d="M18.5588 19.5488C20.6672 17.7154 22 15.0134 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 15.0134 3.33284 17.7154 5.44116 19.5488C7.19693 21.0756 9.49052 22 12 22C14.4162 22 16.6323 21.143 18.3609 19.7165C18.4276 19.6614 18.4936 19.6055 18.5588 19.5488ZM12.2579 19.9959C12.1723 19.9986 12.0863 20 12 20C11.9914 20 11.9827 20 11.9741 20C11.8937 19.9997 11.8135 19.9983 11.7337 19.9956C10.3914 19.9517 9.13273 19.5772 8.03655 18.9508C8.95181 17.7632 10.3882 17 12 17C13.6118 17 15.0482 17.7632 15.9634 18.9508C14.865 19.5785 13.6033 19.9533 12.2579 19.9959ZM17.5624 17.7498C16.2832 16.0781 14.2675 15 12 15C9.73249 15 7.7168 16.0781 6.43759 17.7498C4.93447 16.2953 4 14.2568 4 12C4 7.58172 7.58172 4 12 4C16.4183 4 20 7.58172 20 12C20 14.2568 19.0655 16.2953 17.5624 17.7498Z"
                            fill={context.active === "managestaff" ? "#1A1D1F" : "#6F767E"}
                        />
                    </svg>
                    <span>Manage Staff</span>
                </div>

                <div
                    className={
                        context.active === "managepackage" ? "active-button" : "button"
                    }
                    onClick={() => {
                        context.changeEditActive('');
                        context.addClientId("");
                        context.addClientData(null);
                        context.addOrderData(null);
                        context.addOrderId("");
                        handleDropDown("managepackage");
                    }}
                >
                    <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                        <path
                            d="M21 16V8.00002C20.9996 7.6493 20.9071 7.30483 20.7315 7.00119C20.556 6.69754 20.3037 6.44539 20 6.27002L13 2.27002C12.696 2.09449 12.3511 2.00208 12 2.00208C11.6489 2.00208 11.304 2.09449 11 2.27002L4 6.27002C3.69626 6.44539 3.44398 6.69754 3.26846 7.00119C3.09294 7.30483 3.00036 7.6493 3 8.00002V16C3.00036 16.3508 3.09294 16.6952 3.26846 16.9989C3.44398 17.3025 3.69626 17.5547 4 17.73L11 21.73C11.304 21.9056 11.6489 21.998 12 21.998C12.3511 21.998 12.696 21.9056 13 21.73L20 17.73C20.3037 17.5547 20.556 17.3025 20.7315 16.9989C20.9071 16.6952 20.9996 16.3508 21 16Z"
                            stroke={
                                context.active === "managepackage" ? "#1A1D1F" : "#6F767E"
                            }
                            stroke-width="2"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                        />
                        <path
                            d="M3.27002 6.96002L12 12.01L20.73 6.96002"
                            stroke={
                                context.active === "managepackage" ? "#1A1D1F" : "#6F767E"
                            }
                            stroke-width="2"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                        />
                        <path
                            d="M12 22.08V12"
                            stroke={
                                context.active === "managepackage" ? "#1A1D1F" : "#6F767E"
                            }
                            stroke-width="2"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                        />
                    </svg>
                    <span>Manage Package</span>
                </div>
                <div
                    className={
                        context.active === "managedepartment" ? "active-button" : "button"
                    }
                    onClick={() => {
                        context.changeEditActive('');
                        context.addClientId("");
                        context.addClientData(null);
                        context.addOrderData(null);
                        context.addOrderId("");
                        handleDropDown("managedepartment");
                    }}
                >
                    <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                        <path
                            d="M21 16V8.00002C20.9996 7.6493 20.9071 7.30483 20.7315 7.00119C20.556 6.69754 20.3037 6.44539 20 6.27002L13 2.27002C12.696 2.09449 12.3511 2.00208 12 2.00208C11.6489 2.00208 11.304 2.09449 11 2.27002L4 6.27002C3.69626 6.44539 3.44398 6.69754 3.26846 7.00119C3.09294 7.30483 3.00036 7.6493 3 8.00002V16C3.00036 16.3508 3.09294 16.6952 3.26846 16.9989C3.44398 17.3025 3.69626 17.5547 4 17.73L11 21.73C11.304 21.9056 11.6489 21.998 12 21.998C12.3511 21.998 12.696 21.9056 13 21.73L20 17.73C20.3037 17.5547 20.556 17.3025 20.7315 16.9989C20.9071 16.6952 20.9996 16.3508 21 16Z"
                            stroke={
                                context.active === "managedepartment" ? "#1A1D1F" : "#6F767E"
                            }
                            stroke-width="2"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                        />
                        <path
                            d="M3.27002 6.96002L12 12.01L20.73 6.96002"
                            stroke={
                                context.active === "managedepartment" ? "#1A1D1F" : "#6F767E"
                            }
                            stroke-width="2"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                        />
                        <path
                            d="M12 22.08V12"
                            stroke={
                                context.active === "managedepartmente" ? "#1A1D1F" : "#6F767E"
                            }
                            stroke-width="2"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                        />
                    </svg>
                    <span>Manage Department</span>
                </div>
                <div
                    className={
                        context.active === "managewebsitecontent"
                            ? "active-button"
                            : "button"
                    }
                    onClick={() => {
                        context.changeEditActive('');
                        context.addClientId("");
                        context.addClientData(null);
                        context.addOrderData(null);
                        context.addOrderId("");
                        handleDropDown("managewebsitecontent");
                    }}
                >
                    <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                        <path
                            d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"
                            stroke={
                                context.active === "managewebsitecontent"
                                    ? "#1A1D1F"
                                    : "#6F767E"
                            }
                            stroke-width="2"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                        />
                        <path
                            d="M2 12H22"
                            stroke={
                                context.active === "managewebsitecontent"
                                    ? "#1A1D1F"
                                    : "#6F767E"
                            }
                            stroke-width="2"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                        />
                        <path
                            d="M12 2C14.5013 4.73835 15.9228 8.29203 16 12C15.9228 15.708 14.5013 19.2616 12 22C9.49872 19.2616 8.07725 15.708 8 12C8.07725 8.29203 9.49872 4.73835 12 2V2Z"
                            stroke={
                                context.active === "managewebsitecontent"
                                    ? "#1A1D1F"
                                    : "#6F767E"
                            }
                            stroke-width="2"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                        />
                    </svg>

                    <span>Manage Website Content</span>
                    <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                        className="showMore"
                    >
                        <path
                            d="M6.70711 8.29289C6.31658 7.90237 5.68342 7.90237 5.29289 8.29289C4.90237 8.68342 4.90237 9.31658 5.29289 9.70711L6.70711 8.29289ZM12 15L11.2929 15.7071C11.6834 16.0976 12.3166 16.0976 12.7071 15.7071L12 15ZM18.7071 9.70711C19.0976 9.31658 19.0976 8.68342 18.7071 8.29289C18.3166 7.90237 17.6834 7.90237 17.2929 8.29289L18.7071 9.70711ZM5.29289 9.70711L11.2929 15.7071L12.7071 14.2929L6.70711 8.29289L5.29289 9.70711ZM12.7071 15.7071L18.7071 9.70711L17.2929 8.29289L11.2929 14.2929L12.7071 15.7071Z"
                            fill="#6F767E"
                        />
                    </svg>
                </div>

                <div
                    className={context.active === "logout" ? "active-button" : "button"}
                    onClick={() => {
                        logout();
                    }}
                >
                    <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                        <path
                            d="M15 3H19C19.5304 3 20.0391 3.21071 20.4142 3.58579C20.7893 3.96086 21 4.46957 21 5V19C21 19.5304 20.7893 20.0391 20.4142 20.4142C20.0391 20.7893 19.5304 21 19 21H15"
                            stroke="#F24242"
                            stroke-width="2"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                        />
                        <path
                            d="M10 17L15 12L10 7"
                            stroke="#F24242"
                            stroke-width="2"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                        />
                        <path
                            d="M15 12H3"
                            stroke="#F24242"
                            stroke-width="2"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                        />
                    </svg>

                    <span
                        style={{
                            color: "#F24242",
                        }}
                    >
                        Logout
                    </span>
                </div>
            </div>
        </div>
    );
};

export default Sidebar;