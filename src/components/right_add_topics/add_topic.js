import React, { useState, useEffect } from 'react';
import "./add_topic.css";
import trash from "../../assets/icons/trash.svg";
import close from "../../assets/icons/close.svg";
import axios from 'axios';
import { toast } from "react-toastify";
import edit from "../../assets/icons/edit.svg";

const RightAddTopic = () => {
    const [loading, setLoading] = useState(false);
    const [isEditActive, setIsEditActive] = useState(false);
    const [topicId, setTopicId] = useState("");
    const [topics, setTopics] = useState([]);
    const [topicDetails, setTopicDetails] = useState('');
    const [topicLoading, setTopicLoading] = useState(false);


    const getTopics = async () => {
        try {
            setTopicLoading(true);
            const response = await axios('https://k82gsppxd4.execute-api.ap-south-1.amazonaws.com/latest/api/topic/gettopic');
            if (response.data !== undefined) {
                setTopics(response.data.topics);
                setTopicLoading(false);
            }
        } catch (error) {
            console.log(error);
        }

    };


    const createTopic = async () => {
        try {
            setLoading(true);
            const response = await axios.post('https://k82gsppxd4.execute-api.ap-south-1.amazonaws.com/latest/api/topic/createtopic', { topic: topicDetails });
            if (response.status === 200) {
                toast.success("Topic created successfully.");
                setTopicDetails("");
                getTopics();
            }
            setLoading(false);
        } catch (error) {
            toast.error('Failed to create topic.');
            setLoading(false);
            console.log(error);
        }
    };


    const deleteTopic = async (id) => {
        try {
            const response = await axios.delete(`https://k82gsppxd4.execute-api.ap-south-1.amazonaws.com/latest/api/topic/deletetopic/${id}`);
            if (response.status === 200) {
                toast.success('Topic deleted successfully.');
                getTopics();
            }
        } catch (error) {
            toast.error('Failed to delete topic.');
            console.log(error);
        }
    };

    const updateTopic = async () => {
        try {
            setLoading(true);
            const response = await axios.patch(`https://k82gsppxd4.execute-api.ap-south-1.amazonaws.com/latest/api/topic/update/${topicId}`, { topic: topicDetails });
            if (response.status === 200) {
                toast.success('Topic updated successfully.');
                setIsEditActive(false);
                setTopicDetails("");
                getTopics();
                setLoading(false);
            }
        } catch (error) {
            toast.error('Failed to update topic.');
            console.log(error);
        }
    };

    const handleEdit = (data) => {
        setIsEditActive(true);
        setTopicId(data._id);
        setTopicDetails(data.topic);
    };


    const cancelEdit = () => {
        setIsEditActive(false);
        setTopicId("");
        setTopicDetails("");
    };


    useEffect(() => {
        getTopics();
    }, []);

    const validateData = () => {
        if (topicDetails === "") {
            toast.error("Topic cannot be empty");
        } else {
            createTopic();
        }
    };

    const editPopUp = () => {
        return (
            <div className="edit-container">
                <div className="heading">
                    <div className="left-side">
                        <div className="sidebar"></div>
                        <span>Edit Topic</span>
                    </div>
                    <img src={close} alt="close" onClick={() => {
                        cancelEdit();
                    }} />
                </div>
                <div className="form">
                    <span>Selected Topic</span>
                    <input type="text" placeholder='Mern Stack' value={topicDetails} onChange={(e) => {
                        setTopicDetails(e.target.value);
                    }} />
                </div>
                <div className="btn-container">
                    <button onClick={() => {
                        updateTopic();
                    }}>{loading ? "Loading..." : "Update"}</button>
                </div>
            </div>
        );
    };


    return (
        <div className="add-topic-container">
            <h1>Add Topics</h1>
            <div className="add-topic-form-container">
                <div className="heading">
                    <div className="sidebar"></div>
                    <span>Add Topics</span>
                </div>
                <span className="sub-heading">This topics can be used to define skills</span>

                <div className="form">
                    <input type="text" placeholder='Topic Here' onChange={(e) => {
                        setTopicDetails(e.target.value);
                    }}
                        value={isEditActive ? "" : topicDetails}
                    />
                    <button onClick={() => {
                        validateData();
                    }}>{loading ? <i class="fa fa-circle-o-notch fa-spin"></i> : "+"}</button>
                </div>
            </div>
            <div className="list-topic">
                <div className="heading">
                    <div className="sidebar"></div>
                    <span>Added Topics</span>
                </div>
                {topicLoading ? <div style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    marginTop: "100px"
                }}>
                    <i class="fa fa-circle-o-notch fa-spin fa-5x" style={{
                        color: "#FF6813"
                    }}></i>
                </div> : <div className="topic-container">
                    <table>
                        <tr>
                            <th>SR.NO</th>
                            <th>Topic</th>
                            <th>OPTIONS</th>
                        </tr>

                        {topics.map((data, index) => {
                            return (
                                <tr>
                                    <td>{index + 1}</td>
                                    <td>{data.topic}</td>
                                    <td>
                                        <div className="options">
                                            <img src={edit} alt="trash" onClick={() => {
                                                handleEdit(data);
                                            }} />
                                            <img src={trash} alt="trash" onClick={() => {
                                                deleteTopic(data._id);
                                            }} />
                                        </div>
                                    </td>
                                </tr>
                            );
                        })}

                    </table>
                </div>}
            </div>

            {isEditActive ? editPopUp() : null}
        </div>
    );
};

export default RightAddTopic;