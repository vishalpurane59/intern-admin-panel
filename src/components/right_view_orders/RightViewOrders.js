import React, { useState, useEffect, useContext } from 'react';
import "./RightViewOrders.css";
import trash from "../../assets/icons/trash.svg";
import axios from 'axios';
import { toast } from "react-toastify";
import DataContext from '../../store/DataProvider';
import close from "../../assets/icons/close.svg";
import view from "../../assets/icons/View.svg";
import edit from "../../assets/icons/edit.svg";


const RightViewOrders = () => {
    const [loading, setLoading] = useState(false);
    const [orders, setOrders] = useState([]);
    const [viewData, setViewData] = useState(null);
    const [isViewActive, setIsViewActive] = useState(false);
    const context = useContext(DataContext);

    useEffect(() => {
        getOrders();
    }, []);

    const getOrders = async () => {
        try {
            setLoading(true);
            const response = await axios('https://k82gsppxd4.execute-api.ap-south-1.amazonaws.com/latest/api/order/');
            if (response.data !== undefined) {
                setOrders(response.data.data);
                setLoading(false);
            }
        } catch (error) {
            console.log(error);
        }

    };


    const deleteOrder = async (id) => {
        try {
            const response = await axios.delete(`https://k82gsppxd4.execute-api.ap-south-1.amazonaws.com/latest/api/order/remove/${id}`);
            if (response.status === 200) {
                toast.success('Order deleted successfully.');
                getOrders();
            }
        } catch (error) {
            toast.error('Failed to delete order.');
            console.log(error);
        }
    };


    const handleEditBtn = (data) => {
        context.changeEditActive('order-edit');
        context.addOrderId(data._id);
        context.addOrderData(data);
        context.changeActive('generateorders');
    };


    const handleView = (data) => {
        setIsViewActive(true);
        setViewData(data);
    };

    const viewOrderPopUp = () => {
        return (
            <div className="view-client-container">
                <div className="heading">
                    <div className="left-side">
                        <div className="sidebar"></div>
                        <span>View Order</span>
                    </div>
                    <img src={close} alt="close" onClick={() => {
                        setIsViewActive(false);
                        setViewData(null);
                    }} />
                </div>

                <div className="client-data-container">
                    <div className="input">
                        <span>Client</span>
                        <div className="data">
                            <span>{viewData.clientId?.companyName}</span>
                        </div>
                    </div>
                    <div className="input">
                        <span>Invoice Number</span>
                        <div className="data">
                            <span>{viewData.invoiceNumber}</span>
                        </div>
                    </div>
                    <div className="input">
                        <span>Discount</span>
                        <div className="data">
                            <span>{viewData.discount}</span>
                        </div>
                    </div>
                    <div className="input">
                        <span>Final Amount</span>
                        <div className="data">
                            <span>{viewData.finalAmount}</span>
                        </div>
                    </div>
                    <div className="input">
                        <span>Total Amount</span>
                        <div className="data">
                            <span>{viewData.totalAmount}</span>
                        </div>
                    </div>
                    <div className="input">
                        <span>Package</span>
                        <div className="data">
                            <span>{viewData.packageId.name}</span>
                        </div>
                    </div>
                    <div className="input">
                        <span>Particulars</span>
                        <div className="data">
                            <span>{viewData.particulars}</span>
                        </div>
                    </div>

                </div>
                <div className="tan-gst-container">
                    <span>GSTIN: <span className='gst'>{viewData.gst}</span></span>
                </div>
            </div >
        );
    };



    return (
        <div className="list-orders">
            <div className="heading">
                <div className="sidebar"></div>
                <span>View Orders</span>
            </div>
            {loading ? <div style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                marginTop: "200px"
            }}>
                <i class="fa fa-circle-o-notch fa-spin fa-5x" style={{
                    color: "#FF6813"
                }}></i>
            </div> : <div className="staff-container">
                <table>
                    <tr>
                        <th>SR.NO</th>
                        <th>Invoice Number</th>
                        <th>Discount</th>
                        <th>Final Amount</th>
                        <th>OPTION</th>
                    </tr>

                    {orders.map((data, index) => {
                        return (
                            <tr>
                                <td>{index + 1}</td>
                                <td>{data.invoiceNumber}</td>
                                <td>{data.discount}</td>
                                <td>{data.finalAmount}</td>
                                <td>
                                    <div className="options">
                                        <img src={edit} alt="trash" onClick={() => {
                                            handleEditBtn(data);
                                        }} />
                                        <img src={trash} alt="trash" onClick={() => {
                                            deleteOrder(data._id);
                                        }} />
                                        <img src={view} alt="view" onClick={() => {
                                            handleView(data);
                                        }} />
                                    </div>
                                </td>
                            </tr>
                        );
                    })}

                </table>
            </div>}

            {isViewActive ? viewOrderPopUp() : null}
        </div>
    );
};

export default RightViewOrders;