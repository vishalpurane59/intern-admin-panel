import React, { useState, useEffect } from 'react';
import "./RightDepartment.css";
import axios from 'axios';
import { toast } from "react-toastify";

const RightAddDepartment = () => {
    const [loading, setLoading] = useState(false);
    const [isEditActive, setIsEditActive] = useState(false);
    const [departments, setDepartments] = useState([]);
    const [departmentId, setDepartmentId] = useState("");
    const [departmentLoading, setDepartmentLoading] = useState(false);

    const [departmentDetails, setDepartmentDetails] = useState({
        department: "",
    });

    useEffect(() => {
        getDepartments();
    }, []);

    const getDepartments = async () => {
        try {
            setDepartmentLoading(true);
            const response = await axios('https://k82gsppxd4.execute-api.ap-south-1.amazonaws.com/latest/api/department');
            if (response.status === 200) {
                setDepartments(response.data.data);
                setDepartmentLoading(false);
            }
        } catch (error) {
            console.log(error);
        }
    };


    const createDepartment = async () => {
        try {
            setLoading(true);
            const body = {
                departmentDetails
            };
            const response = await axios.post('https://k82gsppxd4.execute-api.ap-south-1.amazonaws.com/latest/api/department/create', body.departmentDetails);
            setDepartments(prev => [response.data.data, ...prev])
            toast.success("Department created successfully.");
            setDepartmentDetails({
                department: "",
            });
            setLoading(false);
        } catch (error) {
            setLoading(false);
            toast.error("Failed to create department.");
            console.log(error.response.data);
        }
    };



    const updateDepartment = async () => {
        try {
            setLoading(true);
            const body = {
                departmentDetails
            };

            const response = await axios.put(`https://k82gsppxd4.execute-api.ap-south-1.amazonaws.com/latest/api/department/update/${departmentId}`, body.departmentDetails);

            toast.success('Department updated successfully.');
            setIsEditActive(false);
            setDepartmentDetails({
                department: "",
            });
            setLoading(false)
            let updatedData = departments.map(data => {
                if (data._id === response.data.data._id) {
                    data.department = response.data.data.department
                }
                return data
            })
            setDepartments(updatedData)
        } catch (error) {
            toast.error('Failed to update department.');
        }
    };


    const handleEdit = (data) => {
        setIsEditActive(true);
        setDepartmentId(data._id);
        setDepartmentDetails({
            department: data.department,
        });
    };


    const handleChange = (name) => (event) => {
        const value = event.target.value;
        setDepartmentDetails({ ...departmentDetails, [name]: value });
    };


    const deleteDepartment = async (id) => {
        try {
            await axios.delete(`https://k82gsppxd4.execute-api.ap-south-1.amazonaws.com/latest/api/department/remove/${id}`);
            toast.error('Department deleted successfully.');
            let updatedData = departments.filter(data => data._id !== id)
            setDepartments(updatedData)
        } catch (error) {
            toast.error('Failed to delete department.');
        }
    };

    const cancelEditing = () => {
        setDepartmentDetails({
            department: "",
        });
        setIsEditActive(false)
    }


    return (
        <div className="add-department-container">
            <div className="add-topic-form-container">
                <div className="heading">
                    <div className='title'>
                        <div className="sidebar"></div>
                        <span>{isEditActive ? "Edit Department" : "Add Department"}</span>
                    </div>

                    <div className="form">
                        <input type="text" placeholder='Enter department name' name="department"
                            onChange={handleChange("department")}
                            value={departmentDetails.department}
                        />
                        <button onClick={() => {
                            isEditActive ? updateDepartment() : createDepartment()
                        }}>{loading ? <i class="fa fa-circle-o-notch fa-spin"></i> : isEditActive ? "Update" : "Add"}</button>

                        {isEditActive ? <button onClick={cancelEditing} >Cancel</button> : null}
                    </div>
                </div>
            </div>
            <div className="list-topic">
                <div className="heading">
                    <div className="sidebar"></div>
                    <span>Added Departments</span>
                </div>
                {departmentLoading ? <div className='loading-container'>
                    <i class="fa fa-circle-o-notch fa-spin fa-5x" style={{
                        color: "#FF6813"
                    }}></i>
                </div> : <div className="topic-container">
                    <table>
                        <tr>
                            <th>Sr.No</th>
                            <th>Department</th>
                            <th>Options</th>
                        </tr>

                        {departments.map((data, index) => {
                            return (
                                <tr>
                                    <td>{index + 1}</td>
                                    <td>{data.department}</td>
                                    <td>
                                        <div className="options">
                                            {/* Edit Icon */}
                                            <div className='editIconCircle' title='edit' onClick={() => handleEdit(data)}>
                                                <svg className="editIcon"  width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <g clip-path="url(#clip0_1351_1043)">
                                                        <path d="M9.16666 3.33334H3.33332C2.8913 3.33334 2.46737 3.50893 2.15481 3.82149C1.84225 4.13405 1.66666 4.55798 1.66666 5V16.6667C1.66666 17.1087 1.84225 17.5326 2.15481 17.8452C2.46737 18.1577 2.8913 18.3333 3.33332 18.3333H15C15.442 18.3333 15.8659 18.1577 16.1785 17.8452C16.4911 17.5326 16.6667 17.1087 16.6667 16.6667V10.8333" stroke="blue" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                                        <path d="M15.4167 2.08333C15.7482 1.75181 16.1978 1.56557 16.6667 1.56557C17.1355 1.56557 17.5851 1.75181 17.9167 2.08333C18.2482 2.41485 18.4344 2.86449 18.4344 3.33333C18.4344 3.80217 18.2482 4.25181 17.9167 4.58333L9.99999 12.5L6.66666 13.3333L7.49999 10L15.4167 2.08333Z" stroke="blue" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                                    </g>
                                                    <defs>
                                                        <clipPath id="clip0_1351_1043">
                                                            <rect width="20" height="20" fill="white" />
                                                        </clipPath>
                                                    </defs>
                                                </svg>
                                            </div>

                                            {/* Delete Icon */}
                                            <div className='deleteIconCircle' title='delete' onClick={() => deleteDepartment(data._id)}>
                                                <svg className='deleteIcon' width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M2.5 5H4.16667H17.5" stroke="red" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                                    <path d="M15.8333 5V16.6667C15.8333 17.1087 15.6577 17.5326 15.3451 17.8452C15.0326 18.1577 14.6087 18.3333 14.1666 18.3333H5.83329C5.39127 18.3333 4.96734 18.1577 4.65478 17.8452C4.34222 17.5326 4.16663 17.1087 4.16663 16.6667V5M6.66663 5V3.33333C6.66663 2.89131 6.84222 2.46738 7.15478 2.15482C7.46734 1.84226 7.89127 1.66667 8.33329 1.66667H11.6666C12.1087 1.66667 12.5326 1.84226 12.8451 2.15482C13.1577 2.46738 13.3333 2.89131 13.3333 3.33333V5" stroke="red" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                                    <path d="M8.33337 9.16667V14.1667" stroke="red" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                                    <path d="M11.6666 9.16667V14.1667" stroke="red" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                                </svg>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            );
                        })}

                    </table>
                </div>}
            </div>
        </div>
    );
};

export default RightAddDepartment;