import React, { useState } from 'react';
import "./right_dashboard.css";
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';

const RightDashboard = () => {
    const [value, onChange] = useState(new Date());
    return (
        <div className='right-dashboard-container'>
            <div className="top-card-container">
                <div className="card">
                    <span className="content-one">
                        Clients
                    </span>
                    <span className="content-two">
                        27632
                    </span>
                    <span className="content-three">
                        subheading goes here
                    </span>
                </div>
                <div className="card">
                    <span className="content-one">
                        Users
                    </span>
                    <span className="content-two">
                        2300
                    </span>
                    <span className="content-three">
                        subheading goes here
                    </span>
                </div>
                <div className="card">
                    <span className="content-one">
                        Invites
                    </span>
                    <span className="content-two">
                        12345
                    </span>
                    <span className="content-three">
                        subheading goes here
                    </span>
                </div>
                <div className="card">
                    <span className="content-one">
                        Tests
                    </span>
                    <span className="content-two">
                        12345
                    </span>
                    <span className="content-three">
                        subheading goes here
                    </span>
                </div>
            </div>

            <div className="bottom-cards-container">
                <div className="todo-list-container">
                    <div className="heading">
                        <div className="sidebar"></div>
                        <span>TODO List</span>
                    </div><br /><br />
                    <span className="sub-heading">Enter your daily tasks here to keep track with everything</span>
                    <br />
                    <div className="add-todo-container">
                        <input type="text" placeholder='Your Task Goes Here' />
                        <button>+</button>
                    </div>

                    <div className="todo-container">
                        <div className="todo">
                            <input type="checkbox" />
                            <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
                        </div>
                        <div className="todo">
                            <input type="checkbox" />
                            <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
                        </div>
                        <div className="todo">
                            <input type="checkbox" />
                            <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
                        </div>
                    </div>
                    <div className="completed-todo-container">
                        <div className="todo">
                            <input type="checkbox" checked />
                            <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
                        </div>
                        <div className="todo">
                            <input type="checkbox" checked />
                            <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
                        </div>
                        <div className="todo">
                            <input type="checkbox" checked />
                            <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
                        </div>
                    </div>
                </div>
                <div className="calender-container">
                    <Calendar onChange={onChange} value={value} />
                </div>
            </div>
        </div>
    );
};

export default RightDashboard;