import React, { useState, useEffect } from 'react';
import "./RightManageStaff.css";
import axios from 'axios';
import { toast } from "react-toastify";

const RightManageStaff = () => {
    const [staff, setStaff] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        getStaff();
    }, []);

    const getStaff = async () => {
        try {
            setLoading(true);
            const response = await axios('https://k82gsppxd4.execute-api.ap-south-1.amazonaws.com/latest/api/admin/');
            if (response.data !== undefined) {
                setStaff(response.data.data);
            }
            setLoading(false);
        } catch (error) {
            toast.error("Failed to get staff details.");
        }
    }


    return (
        <div className="list-staff">
            <div className="heading">
                <div className="sidebar"></div>
                <span>Manage Staff</span>
            </div>
            {loading ? <div className='loader-container'>
                <i class="fa fa-circle-o-notch fa-spin fa-5x" style={{
                    color: "#FF6813"
                }}></i>
            </div> : <div className="staff-container">
                <table>
                    <tr>
                        <th>Sr.No</th>
                        <th>Email</th>
                        <th>Name</th>
                        <th>Job Role</th>
                        <th>Username</th>
                    </tr>

                    {staff.map((data, index) => {
                        return (
                            <tr>
                                <td>{index + 1}</td>
                                <td>{data.email}</td>
                                <td>{data.fullname}</td>
                                <td>{data.jobrole}</td>
                                <td>{data.username}</td>
                            </tr>
                        );
                    })}

                </table>
            </div>}
        </div>
    );
};

export default RightManageStaff;