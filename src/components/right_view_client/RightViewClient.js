import React, { useState, useEffect, useContext } from 'react';
import "./RightViewClient.css";
import trash from "../../assets/icons/trash.svg";
import axios from 'axios';
import { toast } from "react-toastify";
import DataContext from '../../store/DataProvider';
import close from "../../assets/icons/close.svg";
import avatar from "../../assets/icons/Avatar.svg";
import view from "../../assets/icons/View.svg";
import edit from "../../assets/icons/edit.svg";

const RightViewClient = () => {
    const [loading, setLoading] = useState(false);
    const [clients, setClients] = useState([]);
    const context = useContext(DataContext);
    const [isViewActive, setIsViewActive] = useState(false);
    const [viewData, setViewData] = useState(null);

    useEffect(() => {
        getClients();
    }, []);

    const getClients = async () => {
        try {
            setLoading(true);
            const response = await axios('https://k82gsppxd4.execute-api.ap-south-1.amazonaws.com/latest/api/client/');
            if (response.data !== undefined) {
                setClients(response.data.data);
                setLoading(false);
            }
        } catch (error) {
            console.log(error);
        }

    };


    const deleteClient = async (id) => {
        try {
            const response = await axios.delete(`https://k82gsppxd4.execute-api.ap-south-1.amazonaws.com/latest/api/client/remove/${id}`);
            if (response.status === 200) {
                toast.success('Client deleted successfully.');
                getClients();
            }
        } catch (error) {
            toast.error('Failed to delete client.');
            console.log(error);
        }
    };

    const handleEditBtn = (data) => {
        context.changeEditActive('client-edit');
        context.addClientId(data._id);
        context.addClientData(data);
        context.changeActive('onboardclient');
    };

    const handleView = (data) => {
        setIsViewActive(true);
        setViewData(data);
    };


    const viewClientPopUp = () => {
        return (
            <div className="view-client-container">
                <div className="heading">
                    <div className="left-side">
                        <div className="sidebar"></div>
                        <span>View Client</span>
                    </div>
                    <img src={close} alt="close" onClick={() => {
                        setIsViewActive(false);
                        setViewData(null);
                    }} />
                </div>

                <div className="client-data-container">
                    {viewData.logo !== "" & viewData.logo !== "No Logo" ? <img src={viewData.logo} alt="logo" /> : null}
                    <div className="input">
                        <span>Company Name</span>
                        <div className="data">
                            <span>{viewData.companyName}</span>
                        </div>
                    </div>
                    <div className="input">
                        <span>Company Email</span>
                        <div className="data">
                            <span>{viewData.companyEmail}</span>
                        </div>
                    </div>
                    <div className="input">
                        <span>Phone</span>
                        <div className="data">
                            <span>{viewData.contact}</span>
                        </div>
                    </div>
                    <div className="input">
                        <span>Website</span>
                        <div className="data">
                            <span>{viewData.website}</span>
                        </div>
                    </div>
                    <div className="input">
                        <span>Package</span>
                        <div className="data">
                            <span>{viewData.packageId?.name}</span>
                        </div>
                    </div>
                    <div className="input">
                        <span>Address</span>
                        <div className="data-address">
                            <span>{viewData.companyAddress}</span>
                        </div>
                    </div>
                </div>
                <div className="tan-gst-container">
                    <span>GSTIN: {viewData.gstn}</span>
                    <span>TAN: {viewData.tanNo}</span>
                </div>

                <span className='contact-heading'>Contact Person</span>
                <div className="contact-person-container">
                    {viewData.contactPerson.map((data, index) => {
                        return (
                            <div className="contact-person-card" key={index}>
                                <img src={avatar} alt="avatar" />
                                <span className='name'>{data.name}</span>
                                <span>{data.role}</span>
                                <span>{data.contactno}</span>
                                <span>{data.email}</span>
                            </div>
                        );
                    })}
                </div>
            </div >
        );
    };



    return (
        <div className="list-clients">
            <div className="heading">
                <div className="sidebar"></div>
                <span>View Clients</span>
            </div>
            {loading ? <div style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                marginTop: "200px"
            }}>
                <i class="fa fa-circle-o-notch fa-spin fa-5x" style={{
                    color: "#FF6813"
                }}></i>
            </div> : <div className="staff-container">
                <table>
                    <tr>
                        <th>Sr.No</th>
                        <th>Company Name</th>
                        <th>Email</th>
                        <th>Phone No</th>
                        <th>Options</th>
                    </tr>

                    {clients.map((data, index) => {
                        return (
                            <tr>
                                <td>{index + 1}</td>
                                <td>{data.companyName}</td>
                                <td>{data.companyEmail}</td>
                                <td>{data.contact}</td>
                                <td>
                                    <div className="options">
                                        <img src={edit} alt="trash" onClick={() => {
                                            handleEditBtn(data);
                                        }} />

                                        <img src={trash} alt="trash" onClick={() => {
                                            deleteClient(data._id);
                                        }} />

                                        <img src={view} alt="view" onClick={() => {
                                            handleView(data);
                                        }} />
                                    </div>
                                </td>
                            </tr>
                        );
                    })}

                </table>
            </div>}

            {isViewActive ? viewClientPopUp() : null}
        </div>
    );
};

export default RightViewClient;