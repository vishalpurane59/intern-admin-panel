import React, { useState, useEffect } from 'react';
import "./add_package.css";
import axios from 'axios';
import { toast } from "react-toastify";

const RightAddPackage = () => {
    const [loading, setLoading] = useState(false);
    const [packagesLoading, setpackagesLoading] = useState(true)

    const [isEditActive, setIsEditActive] = useState(false);
    const [packageId, setPackageId] = useState("");
    const [packages, setPackages] = useState([]);

    const [packageDetails, setPackageDetails] = useState({
        name: "",
        noOfInvites: "",
        noOfUsers: "",
        price: "",
        duration: ""
    });

    useEffect(() => {
        getPackages();
    }, []);

    const getPackages = async () => {
        try {
            setpackagesLoading(true)
            const response = await axios('https://k82gsppxd4.execute-api.ap-south-1.amazonaws.com/latest/api/package/');
            if (response.data !== undefined) {
                setPackages(response.data.data);
            }
            setpackagesLoading(false)
        } catch (error) {
            toast.error("Failed to get packages.");
            setpackagesLoading(false)
        }
    }


    const createPackage = async () => {
        try {
            setLoading(true);
            const body = {
                packageDetails
            };
            const response = await axios.post('https://k82gsppxd4.execute-api.ap-south-1.amazonaws.com/latest/api/package/create', body.packageDetails);
            toast.success("Package created successfully.");
            setPackageDetails({
                name: "",
                noOfInvites: "",
                noOfUsers: "",
                price: "",
                duration: ""
            });
            setPackages(prev => [response.data.data, ...prev])
            setLoading(false);
        } catch (error) {
            toast.error('Package name already exists');
            setLoading(false);
        }
    };


    const deletePackage = async (id) => {
        try {
            await axios.delete(`https://k82gsppxd4.execute-api.ap-south-1.amazonaws.com/latest/api/package/remove/${id}`);
            toast.success('Package deleted successfully.');
            let updatedData = packages.filter(data => data._id !== id)
            setPackages(updatedData)
        } catch (error) {
            toast.error('Failed to delete package.');
        }
    };

    const updatePackage = async () => {
        try {
            setLoading(true);
            const body = {
                packageDetails
            };
            const response = await axios.put(`https://k82gsppxd4.execute-api.ap-south-1.amazonaws.com/latest/api/package/update/${packageId}`, body.packageDetails);
            toast.success('Package updated successfully.');
            setIsEditActive(false);
            setPackageDetails({
                name: "",
                noOfInvites: "",
                noOfUsers: "",
                price: "",
                duration: ""
            });
            let updatedData = packages.map(data => {
                if (data._id === packageId) {
                    return response.data.data
                } else {
                    return data
                }
            })
            setPackages(updatedData)
            setLoading(false);
        } catch (error) {
            toast.error('Failed to update package.');
        }
    };

    const handleEdit = (data) => {
        setIsEditActive(true);
        setPackageId(data._id);
        setPackageDetails({
            name: data.name,
            noOfInvites: data.noOfInvites,
            noOfUsers: data.noOfUsers,
            contact: data.contact,
            price: data.price,
            duration: data.duration
        });
    };


    const cancelEdit = () => {
        setIsEditActive(false);
        setPackageId("");
        setPackageDetails({
            name: "",
            noOfInvites: "",
            noOfUsers: "",
            price: "",
            duration: ""
        });
    };


    const handleChange = (name) => (event) => {
        const value = event.target.value;
        setPackageDetails({ ...packageDetails, [name]: value });
    };

    const validateData = () => {
        if (packageDetails.name === "") {
            toast.error("Package name cannot be empty.");
        } else if (packageDetails.noOfInvites === "") {
            toast.error("Invites cannot be empty.");
        } else if (packageDetails.noOfUsers === "") {
            toast.error("No of users cannot be empty.");
        } else if (packageDetails.price === "") {
            toast.error("Price cannot be empty.");
        } else if (packageDetails.duration === "") {
            toast.error("Duration cannot be empty.");
        } else {
            if (isEditActive) {
                updatePackage()
            } else {
                createPackage();
            }
        }
    };

    return (
        <div className="add-order-container">
            <div className="form-container">
                <div className="heading">
                    <div className='title'>
                        <div className="sidebar"></div>
                        <span>Package Form</span>
                    </div>
                    <div className="buttons-container">
                        {isEditActive ? <button title='Cancel' onClick={cancelEdit}>Cancel</button> : null}
                        <button title={isEditActive ? "Update" : "Save"} onClick={validateData}>{loading ? "Loading..." : isEditActive ? "Update" : "Save"}</button>
                    </div>
                </div>
                <div className="form">
                    <div className="input">
                        <span>Package</span>
                        <input type="text" placeholder='Package name here'
                            onChange={handleChange("name")}
                            value={packageDetails.name} />
                        <div className='errorMessage'></div>
                    </div>
                    <div className="input">
                        <span>Invites</span>
                        <input type="number" placeholder='No. of invites'
                            onChange={handleChange("noOfInvites")}
                            value={packageDetails.noOfInvites} />
                    </div>
                    <div className="input">
                        <span>Users</span>
                        <input type="number" placeholder='No. of users'
                            onChange={handleChange("noOfUsers")}
                            value={packageDetails.noOfUsers} />
                    </div>

                    <div className="input">
                        <span>Price (In INR)</span>
                        <input type="number" placeholder='Price goes here'
                            onChange={handleChange("price")}
                            value={packageDetails.price} />
                    </div>
                    <div className="input">
                        <span>Duration (In Days)</span>
                        <input type="number" placeholder='Duration goes here' onChange={handleChange("duration")}
                            value={packageDetails.duration} />
                    </div>
                </div>
            </div>

            <div className="list-package">
                <div className="heading">
                    <div className="sidebar"></div>
                    <span>Added Packages</span>
                </div>
                {
                    packagesLoading ? <div className='loading-container'>
                        <i class="fa fa-circle-o-notch fa-spin fa-5x" style={{ color: "#FF6813" }}></i>
                    </div> :
                        <div className="topic-container">
                            <table>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Name</th>
                                    <th>Invites</th>
                                    <th>Users</th>
                                    <th>Price (In INR)</th>
                                    <th>Duration (In Days)</th>
                                    <th>Options</th>
                                </tr>

                                {packages.map((data, index) => {
                                    return (
                                        <tr>
                                            <td>{index + 1}</td>
                                            <td>{data.name}</td>
                                            <td>{data.noOfInvites}</td>
                                            <td>{data.noOfUsers}</td>
                                            <td>{data.price}</td>
                                            <td>{data.duration}</td>
                                            <td>
                                                <div className="options">
                                                    {/* Edit Icon */}
                                                    <div className='editIconCircle' title='edit' onClick={() => handleEdit(data)}>
                                                        <svg className="editIcon" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <g clip-path="url(#clip0_1351_1043)">
                                                                <path d="M9.16666 3.33334H3.33332C2.8913 3.33334 2.46737 3.50893 2.15481 3.82149C1.84225 4.13405 1.66666 4.55798 1.66666 5V16.6667C1.66666 17.1087 1.84225 17.5326 2.15481 17.8452C2.46737 18.1577 2.8913 18.3333 3.33332 18.3333H15C15.442 18.3333 15.8659 18.1577 16.1785 17.8452C16.4911 17.5326 16.6667 17.1087 16.6667 16.6667V10.8333" stroke="blue" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                                                <path d="M15.4167 2.08333C15.7482 1.75181 16.1978 1.56557 16.6667 1.56557C17.1355 1.56557 17.5851 1.75181 17.9167 2.08333C18.2482 2.41485 18.4344 2.86449 18.4344 3.33333C18.4344 3.80217 18.2482 4.25181 17.9167 4.58333L9.99999 12.5L6.66666 13.3333L7.49999 10L15.4167 2.08333Z" stroke="blue" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                                            </g>
                                                            <defs>
                                                                <clipPath id="clip0_1351_1043">
                                                                    <rect width="20" height="20" fill="white" />
                                                                </clipPath>
                                                            </defs>
                                                        </svg>
                                                    </div>

                                                    {/* Delete Icon */}
                                                    <div className='deleteIconCircle' title='delete' onClick={() => deletePackage(data._id)}>
                                                        <svg className='deleteIcon' width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M2.5 5H4.16667H17.5" stroke="red" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                                            <path d="M15.8333 5V16.6667C15.8333 17.1087 15.6577 17.5326 15.3451 17.8452C15.0326 18.1577 14.6087 18.3333 14.1666 18.3333H5.83329C5.39127 18.3333 4.96734 18.1577 4.65478 17.8452C4.34222 17.5326 4.16663 17.1087 4.16663 16.6667V5M6.66663 5V3.33333C6.66663 2.89131 6.84222 2.46738 7.15478 2.15482C7.46734 1.84226 7.89127 1.66667 8.33329 1.66667H11.6666C12.1087 1.66667 12.5326 1.84226 12.8451 2.15482C13.1577 2.46738 13.3333 2.89131 13.3333 3.33333V5" stroke="red" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                                            <path d="M8.33337 9.16667V14.1667" stroke="red" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                                            <path d="M11.6666 9.16667V14.1667" stroke="red" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                                        </svg>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    );
                                })}

                            </table>
                        </div>
                }
            </div>
        </div>
    );
};

export default RightAddPackage;