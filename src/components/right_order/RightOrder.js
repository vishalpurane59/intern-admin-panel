import React, { useState, useEffect, useContext } from 'react';
import "./RightOrder.css";
import axios from 'axios';
import { toast } from "react-toastify";
import DataContext from '../../store/DataProvider';

const RightOrder = () => {
    const [loading, setLoading] = useState(false);
    const [clients, setClients] = useState([]);
    const [packages, setPackages] = useState([]);
    const context = useContext(DataContext);


    const [orderDetails, setOrderDetails] = useState({
        clientId: "",
        packageId: "",
        invoiceNumber: "",
        particulars: "",
        amount: "",
        discount: "",
        totalAmount: "",
        gst: 18,
        finalAmount: ""
    });

    const getClients = async () => {
        try {
            const response = await axios('https://k82gsppxd4.execute-api.ap-south-1.amazonaws.com/latest/api/client/');
            if (response.data !== undefined) {
                setClients(response.data.data);
            }
        } catch (error) {
            console.log(error);
        }
    };


    const getPackages = async () => {
        try {
            const response = await axios('https://k82gsppxd4.execute-api.ap-south-1.amazonaws.com/latest/api/package');
            if (response.status === 200) {
                setPackages(response.data.data);
            }
        } catch (error) {
            console.log(error);
        }

    };

    const createOrder = async () => {
        try {
            setLoading(true);
            const body = {
                orderDetails
            };
            const response = await axios.post('https://k82gsppxd4.execute-api.ap-south-1.amazonaws.com/latest/api/order/create', body.orderDetails);
            if (response.status === 200) {
                toast.success("Order created successfully.");
                setOrderDetails({
                    clientId: "",
                    packageId: "",
                    invoiceNumber: "",
                    particulars: "",
                    amount: "",
                    discount: "",
                    totalAmount: "",
                    gst: "",
                    finalAmount: ""
                });
            }
            setLoading(false);
        } catch (error) {
            toast.error("Failed to create order.");
            console.log(error);
        }
    };




    const updateOrder = async () => {
        try {
            setLoading(true);
            const body = {
                orderDetails
            };
            const response = await axios.put(`https://k82gsppxd4.execute-api.ap-south-1.amazonaws.com/latest/api/order/update/${context.orderId}`, body.orderDetails);
            if (response.status === 200) {
                toast.success('Order updated successfully.');
                context.changeEditActive("");
                setOrderDetails({
                    clientId: "",
                    packageId: "",
                    invoiceNumber: "",
                    particulars: "",
                    amount: "",
                    discount: "",
                    totalAmount: "",
                    gst: "",
                    finalAmount: ""
                });
                setLoading(false);
            }
        } catch (error) {
            toast.error('Failed to update client.');
            console.log(error);
        }
    };


    const cancelEdit = () => {
        context.changeEditActive("");
        context.addOrderId("");
        context.changeActive("vieworders");
        setOrderDetails({
            clientId: "",
            packageId: "",
            invoiceNumber: "",
            particulars: "",
            amount: "",
            discount: "",
            totalAmount: "",
            gst: "",
            finalAmount: ""
        });
    };




    useEffect(() => {
        getClients();
        getPackages();
        calculateDiscount();
        if (context.editActive === "order-edit") {
            setOrderDetails({
                clientId: context.orderData.clientId,
                packageId: context.orderData.packageId,
                invoiceNumber: context.orderData.invoiceNumber,
                particulars: context.orderData.particulars,
                amount: context.orderData.amount,
                discount: context.orderData.discount,
                totalAmount: context.orderData.totalAmount,
                gst: context.orderData.gst,
                finalAmount: context.orderData.finalAmount
            });
        }

    }, [context.editActive === "order-edit", orderDetails.discount, orderDetails.amount]); // eslint-disable-line react-hooks/exhaustive-deps


    const calculateDiscount = () => {
        let price = Number(orderDetails.amount);
        let discount = Number(orderDetails.discount) / 100;
        let totalAmount = price - (price * discount);
        let gst = 18 / 100;
        let finalAmount = totalAmount + (totalAmount * gst);
        setOrderDetails({ ...orderDetails, totalAmount: totalAmount, finalAmount: finalAmount });
    };

    return (
        <div className="add-client-container">
            <h1>Add Order Form</h1>
            <div className="form-container">

                <div className="form">
                    <div className="input">
                        <span>Invoice Number</span>
                        <input type="text" placeholder='Invoice Number'
                            onChange={(e) => {
                                setOrderDetails({ ...orderDetails, invoiceNumber: e.target.value });
                            }}
                            value={orderDetails.invoiceNumber} />
                    </div>
                    <div className="input">
                        <span>Client</span>
                        <select name="client" onChange={(e) => {
                            setOrderDetails({ ...orderDetails, clientId: e.target.value });
                        }}>
                            <option value="">Select Client</option>
                            {clients.map((data, index) => {
                                return <option value={data._id} key={index} selected={orderDetails.clientId._id === data._id ? "selected" : null}>{data.companyName}</option>;
                            })}
                        </select>
                    </div>
                    <div className="input">
                        <span>Package</span>
                        <select name="package" onChange={(e) => {
                            setOrderDetails({ ...orderDetails, packageId: e.target.value });
                        }}>
                            <option value="">Select Package</option>
                            {packages.map((data, index) => {
                                return <option value={data._id} key={index} selected={orderDetails.packageId._id === data._id ? "selected" : null}>{data.name}</option>;
                            })}
                        </select>
                    </div>
                </div>

                <div className="form">
                    <div className="input">
                        <span>Amount (In INR)</span>
                        <input type="number" placeholder="Amount" onChange={(e) => {
                            setOrderDetails({ ...orderDetails, amount: e.target.value });
                        }}
                            value={orderDetails.amount} />
                    </div>
                    <div className="input">
                        <span>Discount (In %)</span>
                        <input type="number" placeholder='Discount' onChange={(e) => {
                            setOrderDetails({ ...orderDetails, discount: e.target.value });
                        }}
                            value={orderDetails.discount} maxlength="3" />
                    </div>

                    <div className="input">
                        <span>Total Amount (In INR)</span>
                        <input type="number" placeholder='Total amount' value={orderDetails.totalAmount} disabled />
                    </div>
                </div>
                <div className="form2">
                    <div className="input">
                        <span>Particulars</span>
                        <textarea name="particulars" onChange={(e) => {
                            setOrderDetails({ ...orderDetails, particulars: e.target.value });
                        }}
                            value={orderDetails.particulars} placeholder="Particulars"></textarea>
                    </div>
                    <div className="gstn-container">
                        <div className="input" style={{
                            marginBottom: "30px"
                        }}>
                            <span>GST (9% IGST + 9% CGST)</span>
                            <input type="text" value={orderDetails.gst} disabled />
                        </div>
                        <div className="input">
                            <span>Final Amount (In INR)</span>
                            <input type="number" placeholder='Final Amount' onChange={(e) => {
                                setOrderDetails({ ...orderDetails, finalAmount: e.target.value });
                            }}
                                value={orderDetails.finalAmount} disabled />
                        </div>
                    </div>
                </div>
                <div className="buttons-container">
                    {context.editActive === "order-edit" ? <button onClick={cancelEdit}>Cancel</button> : null}
                    <button onClick={context.editActive === "order-edit" ? updateOrder : createOrder}>{loading ? "Loading..." : context.editActive === "order-edit" ? "Update" : "Save"}</button>
                </div>
            </div>
        </div>
    );
};

export default RightOrder;