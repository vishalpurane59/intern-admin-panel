import React, { useContext } from 'react';
import "./Dashboard.css";
import Navbar from '../../components/navbar/Navbar';
import Sidebar from '../../components/left-sidebar/Sidebar';
import DataContext from '../../store/DataProvider';
import RightDashboard from '../../components/right-dashboard/right_dashboard';
import RightAddClient from '../../components/right_add_client/add_client';
import RightAddPackage from '../../components/right_add_package/add_package';
import RightAddSkill from '../../components/right_add_skill/add_skill';
import RightOrder from '../../components/right_order/RightOrder';
import RightViewClient from '../../components/right_view_client/RightViewClient';
import RightViewOrders from '../../components/right_view_orders/RightViewOrders';
import RightAddTopic from '../../components/right_add_topics/add_topic';
import RightManageStaff from '../../components/right_manage_staff/RightManageStaff';
import RightAddJobRole from '../../components/right_add_job_role/RightAddJobRole';
import RightAddDepartment from '../../components/right_add_department/RightDepartment';


const Dashboard = () => {
    const context = useContext(DataContext);
    return (
        <div>
            <Navbar />
            <div className='dashboard-container'>
                <Sidebar />
                <div className="rightside-container">
                    {context.active === 'home' ? <RightDashboard /> : null}
                    {context.active === 'onboardclient' || context.active === 'manageclient' ? <RightAddClient /> : null}
                    {context.active === 'managepackage' ? <RightAddPackage /> : null}
                    {context.active === 'skills' ? <RightAddSkill /> : null}
                    {context.active === "manageorders" || context.active === 'generateorders' ? <RightOrder /> : null}
                    {context.active === "viewclient" ? <RightViewClient /> : null}
                    {context.active === "vieworders" ? <RightViewOrders /> : null}
                    {context.active === "managestaff" ? <RightManageStaff /> : null}
                    {context.active === "roles" || context.active === "librarysettings" ? <RightAddJobRole /> : null}
                    {context.active === "topics" ? <RightAddTopic /> : null}
                    {context.active === "managedepartment" ? <RightAddDepartment /> : null}
                </div>
            </div>
        </div>

    );
};

export default Dashboard;
