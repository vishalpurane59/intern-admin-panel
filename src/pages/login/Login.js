import React, { useState, useEffect } from 'react';
import Styles from "./login.module.css";
import EliteQALogo from "./../../assets/logo.svg";
import thumbUp from "../../assets/images/Group_5.svg";
import avatarOne from "../../assets/images/roundAvatarOne.svg";
import avatarTwo from "../../assets/images/roundAvatarTwo.svg";
import avatarThree from "../../assets/images/roundAvatarThree.svg";
import threeDots from "../../assets/images/threeDots.svg";
import ellipseOne from "../../assets/images/ellipseOne.svg";
import ellipseTwo from "../../assets/images/ellipseTwo.svg";
import { useNavigate } from "react-router-dom";
import axios from 'axios';
import EmailIcon from '../../assets/icons/email.svg';
import lock from '../../assets/icons/lock.svg';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';



function Login(props) {
    let navigate = useNavigate();
    const [email, setemail] = useState("vishalpurane59@gmail.com");
    const [password, setpassword] = useState("Vishal@67");
    const [loading, setloading] = useState(false);
    const [passwordView, setpasswordView] = useState(false);
    const [emailStyle, setEmailStyle] = useState({});
    const [passwordStyle, setPasswordStyle] = useState({});
    const isLoggedIn = localStorage.getItem("token");



    useEffect(() => {
        if (isLoggedIn) {
            navigate("/dashboard");
        }
    }, [isLoggedIn]); // eslint-disable-line react-hooks/exhaustive-deps

    const loginUser = async () => {
        try {
            setloading(true);
            if (email === "") {
                toast("Hey! email can't be empty!");
                setloading(false);
            } else if (password === "") {
                toast("Hey! password can't be empty!");
                setloading(false);
            } else {
                const res = await axios.post(`https://k82gsppxd4.execute-api.ap-south-1.amazonaws.com/latest/api/admin/login`, {
                    email: email,
                    password: password,
                });
                if (res.status === 200) {
                    setloading(false);
                    navigate("/dashboard");
                    localStorage.setItem('token', res.data.token);
                } else if (res.status === 404) {
                    setloading(false);
                    toast("Invalid username or password!");
                }
            }
        }
        catch (error) {
            setloading(false);
            toast(error.response.data.error);
        }
    };


    function isEmail(val) {
        let regEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;//eslint-disable-line
        if (!regEmail.test(val)) {
            return 'Invalid Email';
        }
    }

    const activeEmailInput = (e) => {
        setPasswordStyle({});
        setEmailStyle({ border: '1px solid black' });
    };
    const activePasswordInput = (e) => {
        setEmailStyle({});
        setPasswordStyle({ border: '1px solid black' });
    };
    return (
        <div className={Styles.container}>
            <ToastContainer />
            <div className={Styles.left}>
                <div className={Styles.logo}>
                    <img src={EliteQALogo} alt="" onClick={() => {
                        window.open("https://www.theeliteqa.com/");
                    }} style={{ cursor: "pointer" }} />
                </div>
                <div className={Styles.header}>
                    <div className={Styles.title}>Admin Panel</div>
                    <div className={Styles.subtitle}>
                        Please login here ...
                    </div>
                </div>
                <div className={Styles.form}>
                    <div className={Styles.inputItem} >
                        <label htmlFor="email">Email</label>
                        <div style={emailStyle}>
                            <img src={EmailIcon} alt="" />
                            <input placeholder='Email' className={isEmail(email) === "Invalid Email" && email !== "" ? Styles.invalid : null} onChange={(e) => {
                                setemail(e.target.value);
                            }} value={email} type="email" name="" id="email" onFocus={(e) => {
                                activeEmailInput(e);
                            }} />
                        </div>
                        {email !== "" ?
                            <span>{isEmail(email)}</span> : <></>
                        }
                    </div>
                    <div className={Styles.inputItem}>
                        <label htmlFor="password">Password</label>
                        <div style={passwordStyle}>
                            <img src={lock} alt="" />
                            <input placeholder='Password' type={passwordView ? "text" : "password"} onChange={(e) => {
                                setpassword(e.target.value);

                            }} value={password} name="" id="password" onFocus={(e) => {
                                activePasswordInput(e);
                            }} />
                            {passwordView ?
                                <svg onClick={() => setpasswordView(!passwordView)} width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 12C1 12 5 4 12 4C19 4 23 12 23 12C23 12 19 20 12 20C5 20 1 12 1 12Z" stroke="black" stroke-linecap="round" stroke-linejoin="round" />
                                    <path d="M12 15C13.6569 15 15 13.6569 15 12C15 10.3431 13.6569 9 12 9C10.3431 9 9 10.3431 9 12C9 13.6569 10.3431 15 12 15Z" stroke="black" stroke-linecap="round" stroke-linejoin="round" />
                                </svg>
                                :
                                <svg onClick={() => setpasswordView(!passwordView)} width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 12C1 12 5 4 12 4C19 4 23 12 23 12C23 12 19 20 12 20C5 20 1 12 1 12Z" stroke="#AAAAAA" stroke-linecap="round" stroke-linejoin="round" />
                                    <path d="M12 15C13.6569 15 15 13.6569 15 12C15 10.3431 13.6569 9 12 9C10.3431 9 9 10.3431 9 12C9 13.6569 10.3431 15 12 15Z" stroke="#AAAAAA" stroke-linecap="round" stroke-linejoin="round" />
                                </svg>
                            }

                        </div>

                    </div>
                    <div className={Styles.btn} onClick={loginUser}>
                        {loading ? <div><i class="fa fa-circle-o-notch fa-spin" style={{
                            marginRight: "5px"
                        }}></i>Loading...</div> : "Login"}
                    </div>
                </div>
            </div>
            <div className={Styles.right}>
                <img src={ellipseOne} alt="thumbs up" className={Styles.ellipseOne} />
                <img src={ellipseTwo} alt="thumbs up" className={Styles.ellipseTwo} />
                <div className={Styles.boxOne}>
                    <img src={thumbUp} alt="thumbs up" />
                    <div className={Styles.boxText}>
                        <span className={Styles.textOne}>135</span>
                        <br />
                        <span className={Styles.textTwo}>Companies Using ElteQA</span>
                    </div>
                </div>
                <div className={Styles.imageContainerOne}>
                    <img src={avatarOne} alt="thumbs up" />
                    <img src={avatarTwo} alt="thumbs up" />
                </div>
                <div className={Styles.imageContainerTwo}>
                    <img src={avatarThree} alt="thumbs up" />
                    <div className={Styles.boxTwo}>
                        <div className={Styles.boxText}>
                            <span className={Styles.textOne}>Name here</span>
                            <br />
                            <span className={Styles.textTwo}>
                                Hiring is now peice of cake!
                            </span>
                        </div>
                    </div>
                </div>

                <div className={Styles.subText}>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec arcu
                        ipsum, mollis non erat vitae, vulputate ultrices dui
                    </p>
                    <img src={threeDots} alt="thumbs up" />
                </div>
            </div>
        </div>
    );
}

export default Login;
