import React from 'react';
import { Navigate } from 'react-router-dom';

const ProtectedRoutes = ({ children }) => {
    const isAuthenticated = localStorage.getItem("token");
    if (isAuthenticated === null) {
        return <Navigate to={"/"} />;
    }
    return children;
};

export default ProtectedRoutes;